﻿* Encoding: UTF-8.
* Commande pour l'importation du fichier CSV dans SPSS

* Attention: modifier la ligne ci-dessous commençant par /FILE
* afin d'indiquer le chemin complet du jeu de données
* (p.ex. C:\Users\FooBar\trans_2011_data.csv).

* Importation du fichier

SET UNICODE=ON.
GET DATA
 /TYPE=TXT
 /ENCODING='UTF8'
 /FILE='trans_2011_data_proxy.csv'
 /DELCASE=LINE
 /DELIMITERS=','
 /QUALIFIER='"'
 /ARRANGEMENT=DELIMITED
 /FIRSTCASE=2
 /IMPORTCASE=ALL
 /VARIABLES=
id F3
code_ego F6
code_enqueteur F3
canton F1
genre F1
age_entretien2 F2
age_entretien_groupe F2
qp_a3 F2
qp_a4 F1
qp_a5 F1
qp_a6 F1
qp_a7 F1
qp_a8 F1
qp_a9 F1
qp_a9_autre A50
qp_a10 F2
qp_a10_autre A50
qp_a11 F3
qa_a1 F4
qa_a2 F4
qa_a4 F4
qf_y3 F1
qa_a5_francais F1
qa_a5_allemand F1
qa_a5_italien F1
qa_a5_anglais F1
qa_a5_espagnol F1
qa_a5_portugais F1
qa_a5_turc F1
qa_a5_sercrobos F1
qa_a5_albanais F1
qa_a5_autre F1
qa_a6 A50
qa_a7 F1
qa_a8 F1
qa_a8_autre_txt A50
qa_m1 F2
qa_m2 A150
qa_c1 F1
qa_c2 F2
qa_c3 F2
qa_c4 F1
qf_t1 F1
qf_t2 F2
qf_t3 F2
qa_e1 F1
qa_e2 A50
qa_e5 F2
qa_e6 F2
qa_e7 F1
qa_e7b F1
qa_e7c F1
qa_e8 A100
qa_e9 F1
qf_x1 F2
qf_x1_autre A100
qf_x2 F3
qf_x3 F2
qf_x3_autre A100
qf_x4 F3
qf_ar2 F1
qa_i1 F3
qa_i2 F3
qf_z1_maladie F1
qf_z1_accident F1
qf_z1_operation F1
qf_z8 F1
qf_z9 F2
qf_ac4 F1
qf_ac5 F1
qa_i8_membreinferieur F1
qa_i8_membresuperieur F1
qa_i8_tete F1
qa_i8_dos F1
qa_i8_coeur F1
qa_i8_respiratoire F1
qa_i8_ventre F1
qa_i8_vessiegenital F1
qa_i8_poitrine F1
qa_i8_fievre F1
qa_i8_autre F1
qa_i9_autre_txt A100
qa_i11 F1
qa_i13 F1
qa_i14 F1
qa_i15 F1
qa_i16 F2
qf_ac1_appetit F1
qf_ac1_isole F1
qf_ac1_dormir F1
qf_ac1_plaisir F1
qf_ac1_temps_long F1
qf_ac1_triste F1
qf_ac1_confiance F1
qf_ac1_pleurer F1
qf_ac1_fatigue F1
qf_ac1_sante F1
qf_ac1_irritable F1
qf_ac1_confiance_avenir F1
qf_ac1_anxieux F1
qa_i19_deplacer F1
qa_i19_escalier F1
qa_i19_exterieur F1
qa_i19_marcher200 F1
qa_i19_coucherlever F1
qa_i19_habiller F1
qa_i19_manger F1
qa_i19_laver F1
qf_ac3_continence F1
qf_ac3_telephoner F1
qf_ac3_courses F1
qf_ac3_repas F1
qf_ac3_menage F1
qf_ac3_lessive F1
qf_ac3_transports F1
qf_ac3_medicaments F1
qf_ac3_finances F1
qf_aq3_menage F1
qf_aq3_repas F1
qf_aq3_courses F1
qf_aq3_reparations F1
qf_aq3_remplir_impot F1
qf_aq3_aider_toilette F1
qf_aq3_presence F1
qf_aq3_emmener_promenade F1
qf_aq3_aide_financiere F1
qf_aq7_menage F1
qf_aq7_repas F1
qf_aq7_courses F1
qf_aq7_reparations F1
qf_aq7_remplir_impot F1
qf_aq7_aider_toilette F1
qf_aq7_presence F1
qf_aq7_emmener_personne F1
qa_j1_infirmiere F1
qa_j1_service_aide F1
qa_j1_physiotherapeute F1
qa_j1_assistant_social F1
qa_j1_repas F1
qa_j1_hopital_jour F1
qa_j1_benevole F1
qa_j1_employe_menage F1
qa_j1_autre F1
qa_j2 A50
qa_l1_chezvous F1
qa_l1_chezeux F1
qa_l1_telephone F1
qa_l2_chezvous F1
qa_l2_chezeux F1
qa_l2_telephone F1
qa_f1_radio F1
qa_f1_television F1
qa_f1_journal F1
qa_f1_livre F1
qa_f1_internet F1
qf_at1 F1
qf_at3 F1
qf_at5 F1
qf_at7 F1
qf_at11 F1
qf_at13 F1
qf_at15 F1
qf_at17 F1
qf_at19 F1
qf_at21 F1
qf_at23 F1
qf_at25 F1
qf_at27 F1
qf_at29 F1
qf_at31 F1
qf_at33 F1
qf_at35 F1
qf_at37 F1
qf_at39 F1
qf_at41 F1
qf_at43 F1
qf_at8_sport1 A50
qf_at8_sport2 A50.
CACHE.
EXECUTE.

* Définition des labels de variables

VARIABLE LABELS id "Id".
VARIABLE LABELS code_ego "Code ego".
VARIABLE LABELS code_enqueteur "Code enquêteur".
VARIABLE LABELS canton "Groupe selon canton".
VARIABLE LABELS genre "Genre".
VARIABLE LABELS age_entretien2 "Âge à l'entretien arrondi".
VARIABLE LABELS age_entretien_groupe "Groupe d'âge à l'entretien".
VARIABLE LABELS qp_a3 "Âge proxy".
VARIABLE LABELS qp_a4 "Sexe proxy".
VARIABLE LABELS qp_a5 "Quelle est votre relation avec la personne interrogée?".
VARIABLE LABELS qp_a6 "Vivez-vous avec la personne interrogée?".
VARIABLE LABELS qp_a7 "À quelle fréquence voyez-vous la personne interrogée?".
VARIABLE LABELS qp_a8 "Depuis combien de temps connaissez-vous la personne interrogée?".
VARIABLE LABELS qp_a9 "Quelle est actuellement votre situation professionnelle?".
VARIABLE LABELS qp_a9_autre "Quelle est actuellement votre situation professionnelle? Autre - texte".
VARIABLE LABELS qp_a10 "Quel est (était) votre dernier métier?".
VARIABLE LABELS qp_a10_autre "Quel est (était) votre dernier métier? Autre - texte".
VARIABLE LABELS qp_a11 "Quel est (était) votre taux de travail?".
VARIABLE LABELS qa_a1 "Quelle est votre date de naissance?".
VARIABLE LABELS qa_a2 "Nationalité 1 [Quelle(s) nationalité(s) avez-vous?]".
VARIABLE LABELS qa_a4 "Nationalité 2 [Quelle(s) nationalité(s) avez-vous?]".
VARIABLE LABELS qf_y3 "Actuellement, quel permis avez-vous?".
VARIABLE LABELS qa_a5_francais "Français [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_allemand "Allemand / Suisse-allemand [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_italien "Italien [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_anglais "Anglais [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_espagnol "Espagnol [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_portugais "Portugais [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_turc "Turc [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_sercrobos "Serbe, Croate ou Bosniaque [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_albanais "Albanais [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a5_autre "Autre(s) [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a6 "Autre - texte [Quelle(s) langue(s) parlez-vous habituellement?]".
VARIABLE LABELS qa_a7 "Quel est votre état civil?".
VARIABLE LABELS qa_a8 "Quel est le plus haut niveau de formation que vous avez accompli?".
VARIABLE LABELS qa_a8_autre_txt "Quel est le plus haut niveau de formation que vous avez accompli? Autre - texte".
VARIABLE LABELS qa_m1 "De quelle confession êtes-vous?".
VARIABLE LABELS qa_m2 "De quelle confession êtes-vous? Autre - texte".
VARIABLE LABELS qa_c1 "Avez-vous eu des enfants?".
VARIABLE LABELS qa_c2 "Combien en avez-vous eu? [Enfants]".
VARIABLE LABELS qa_c3 "Actuellement combien comptez-vous encore d'enfants?".
VARIABLE LABELS qa_c4 "Avez-vous des petits-enfants?".
VARIABLE LABELS qf_t1 "Avez-vous eu des frères et sœurs?".
VARIABLE LABELS qf_t2 "Combien avez-vous eu de frères et/ou sœurs?".
VARIABLE LABELS qf_t3 "Actuellement, combien comptez-vous encore de frères et/ou sœurs?".
VARIABLE LABELS qa_e1 "Où habitez-vous?".
VARIABLE LABELS qa_e2 "Où habitez-vous? Autre - texte".
VARIABLE LABELS qa_e5 "Habitez-vous seul(e)?".
VARIABLE LABELS qa_e6 "Combien de personnes vivent avec vous dans le même logement?".
VARIABLE LABELS qa_e7 "Personne 1 [Quelles sont les personnes qui vivent avec vous dans le même logement?]".
VARIABLE LABELS qa_e7b "Personne 2 [Quelles sont les personnes qui vivent avec vous dans le même logement?]".
VARIABLE LABELS qa_e7c "Personne 3 [Quelles sont les personnes qui vivent avec vous dans le même logement?]".
VARIABLE LABELS qa_e8 "Autre - texte [Quelles sont les personnes qui vivent avec vous dans le même logement?]".
VARIABLE LABELS qa_e9 "Disposez-vous d'une chambre individuelle?".
VARIABLE LABELS qf_x1 "Quel était le premier métier que vous avez exercé après votre formation?".
VARIABLE LABELS qf_x1_autre "Quel était le premier métier que vous avez exercé après votre formation? Autre - texte".
VARIABLE LABELS qf_x2 "Quel était votre taux de travail? [Premier métier]".
VARIABLE LABELS qf_x3 "Et quel est le dernier métier que vous avez exercé?".
VARIABLE LABELS qf_x3_autre "Et quel est le dernier métier que vous avez exercé? Autre - texte".
VARIABLE LABELS qf_x4 "Quel était votre taux de travail? [Dernier métier]".
VARIABLE LABELS qf_ar2 "Sur la base de l'échelle de revenus que je vais vous énoncer, pouvez-vous me dire à combien se monte le revenu mensuel total-brut de votre ménage?".
VARIABLE LABELS qa_i1 "Quel est votre poids actuel? [kg]".
VARIABLE LABELS qa_i2 "Quelle est votre taille actuelle? [cm]".
VARIABLE LABELS qf_z1_maladie "D'une maladie? [Actuellement, souffrez-vous:]".
VARIABLE LABELS qf_z1_accident "Des suites d'un accident? [Actuellement, souffrez-vous:]".
VARIABLE LABELS qf_z1_operation "Des suites d'une opération? [Actuellement, souffrez-vous:]".
VARIABLE LABELS qf_z8 "Au cours de la dernière année, avez-vous été hospitalisé(e)?".
VARIABLE LABELS qf_z9 "Combien de fois avez-vous été hospitalisé(e)?".
VARIABLE LABELS qf_ac4 "Avez-vous pris des médicaments au cours de cette dernière semaine?".
VARIABLE LABELS qf_ac5 "Dans la vie de tous les jours, arrive-t-il que votre mémoire vous joue des tours?".
VARIABLE LABELS qa_i8_membreinferieur "Douleurs, crampes, tremblements ou enflure des membres inférieurs [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_membresuperieur "Douleurs, crampes, tremblements ou enflure des membres supérieurs [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_tete "Maux de tête ou autres douleurs au visage [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_dos "Maus de dos, de reins [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_coeur "Irrégularités cardiaques: palpitations et douleurs [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_respiratoire "Difficultés respiratoires, asthme, toux chroniques, maux de gorge [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_ventre "Maux d'estomac, maux de ventre, diarrhée, constipation [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_vessiegenital "Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou du système urinaire [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_poitrine "Douleurs dans la poitrine, sensation de pression [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_fievre "Fièvres [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i8_autre "Autre [Au cours des quatre dernières semaines, avez-vous souffert de:]".
VARIABLE LABELS qa_i9_autre_txt "Veuillez préciser de quoi vous avez souffert:".
VARIABLE LABELS qa_i11 "Est-ce que votre vue est suffisamment bonne pour vous permettre de lire un texte normalement imprimé dans un journal?".
VARIABLE LABELS qa_i13 "Pouvez-vous entendre ce que vous dit une autre personne au cours d'une conversation normale avec elle seule?".
VARIABLE LABELS qa_i14 "Pouvez-vous suivre une conversation à laquelle participent plusieurs personnes ?".
VARIABLE LABELS qa_i15 "Au cours de la dernière année, vous est-il arrivé de tomber?".
VARIABLE LABELS qa_i16 "Combien de fois environ vous est-il arrivé de tomber?".
VARIABLE LABELS qf_ac1_appetit "J'ai bon appétit [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_isole "Je me sens un peu isolé(e), un peu seul(e), même parmi des amis [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_dormir "J'ai de la peine à dormir la nuit [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_plaisir "Je prends plaisir à ce que je fais [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_temps_long "Je trouve le temps long [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_triste "Je me sens triste [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_confiance "J'ai confiance en moi [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_pleurer "J'ai des crises de larmes et envie de pleurer [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_fatigue "Je me sens fatigué(e) [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_sante "Je me fais du souci pour ma santé [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_irritable "Je me sens irritable [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_confiance_avenir "J'ai confiance en l'avenir [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qf_ac1_anxieux "Je me sens anxieux(se), angoissé(e) [Voici une série d'affirmations. Veuillez indiquer dans quelle mesure cela s'applique à vous]".
VARIABLE LABELS qa_i19_deplacer "Se déplacer d'une pièce à l'autre [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qa_i19_escalier "Monter ou descendre un escalier [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qa_i19_exterieur "Se déplacer à l'extérieur du logement [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qa_i19_marcher200 "Parcourir à pied 200 mètres au moins [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qa_i19_coucherlever "Se coucher et se lever [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qa_i19_habiller "S'habiller et se déshabiller [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qa_i19_manger "Manger et couper les aliments [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qa_i19_laver "Faire sa toilette [Pouvez-vous réaliser seul(e):]".
VARIABLE LABELS qf_ac3_continence "Continence [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_telephoner "Téléphoner [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_courses "Faire les courses [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_repas "Préparer les repas [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_menage "Faire le ménage [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_lessive "Faire la lessive [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_transports "Utiliser les transports publics [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_medicaments "Prendre vos médicaments [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_ac3_finances "Gérer vos finances [Pour chacune des activités énoncées, veuillez indiquer si vous êtes en mesure de le faire seul, seul mais avec une aide, ou si vous ne pouvez pas le faire seul]".
VARIABLE LABELS qf_aq3_menage "Faire le ménage chez vous [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_repas "Vous apporter des repas ou préparer des repas chez vous [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_courses "Faire les courses pour vous [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_reparations "Faire des réparations, bricoler, jardiner chez vous [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_remplir_impot "Vous aider à remplir la feuille d'impôt, une déclaration d'assurance, etc. [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_aider_toilette "Vous aider dans votre toilette (bain, coiffure, etc.) [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_presence "Assurer une présence, une compagnie auprès de vous. Apporter un soutien moral, un réconfort [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_emmener_promenade "Vous emmener en promenade, au spectacle, au café ou au restaurant [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq3_aide_financiere "Vous aider financièrement [Pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre membre de votre famille?]".
VARIABLE LABELS qf_aq7_menage "Faire le ménage chez vous [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connaissances?]".
VARIABLE LABELS qf_aq7_repas "Apporter des repas ou préparer des repas chez vous [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connaissances?]".
VARIABLE LABELS qf_aq7_courses "Faire les courses pour vous [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connaissances?]".
VARIABLE LABELS qf_aq7_reparations "Faire des réparations, bricoler, jardiner chez vous [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connaissances?]".
VARIABLE LABELS qf_aq7_remplir_impot "Vous aider à remplir la feuille d'impôt, une déclaration d'assurance, etc. [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connai".
VARIABLE LABELS qf_aq7_aider_toilette "Vous aider dans votre toilette (bain, coiffure, etc.) [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connaissances?]".
VARIABLE LABELS qf_aq7_presence "Vous apporter une présence, une compagnie, un soutien moral [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connaissances?]".
VARIABLE LABELS qf_aq7_emmener_personne "Vous emmener en promenade, au spectacle, au café ou au restaurant [De la même façon, pouvez-vous me dire à quelle fréquence vous recevez ces différents services de l'un ou l'autre de vos ami(e)s, connaissance".
VARIABLE LABELS qa_j1_infirmiere "Infirmier(ère) et‎/ou aide à domicile [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_service_aide "Service d'aide ménagère et‎/ou aide familiale [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_physiotherapeute "Physiothérapeute et‎/ou ergothérapeute [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_assistant_social "Assistant(e) social(e) [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_repas "Repas à domicile [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_hopital_jour "Hôpital de jour [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_benevole "Bénévoles [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_employe_menage "Employé(e) de ménage et‎/ou employé(e) de maison payé(e) par vous ou par votre famille [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j1_autre "Autre [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_j2 "Autre - texte [À quelle fréquence avez-vous reçu de l'aide d'un des services ou personnes suivants au cours des trois derniers mois?]".
VARIABLE LABELS qa_l1_chezvous "Recevoir chez vous les membres de votre famille [Veuillez indiquer à quelle fréquence vous communiquez avec les membres de votre famille]".
VARIABLE LABELS qa_l1_chezeux "Aller chez des membres de votre famille [Veuillez indiquer à quelle fréquence vous communiquez avec les membres de votre famille]".
VARIABLE LABELS qa_l1_telephone "Téléphoner à des membres de votre famille ou recevoir des téléphones de leur part [Veuillez indiquer à quelle fréquence vous communiquez avec les membres de votre famille]".
VARIABLE LABELS qa_l2_chezvous "Recevoir chez vous des amis ou connaissances [Veuillez indiquer à quelle fréquence vous communiquez avec vos amis et vos connaissances]".
VARIABLE LABELS qa_l2_chezeux "Aller chez des amis ou connaissances [Veuillez indiquer à quelle fréquence vous communiquez avec vos amis et vos connaissances]".
VARIABLE LABELS qa_l2_telephone "Téléphoner à des amis ou connaissances ou recevoir des téléphones de leur part [Veuillez indiquer à quelle fréquence vous communiquez avec vos amis et vos connaissances]".
VARIABLE LABELS qa_f1_radio "Radio [À quelle fréquence utilisez-vous les médias suivants?]".
VARIABLE LABELS qa_f1_television "Télévision [À quelle fréquence utilisez-vous les médias suivants?]".
VARIABLE LABELS qa_f1_journal "Journal [À quelle fréquence utilisez-vous les médias suivants?]".
VARIABLE LABELS qa_f1_livre "Livre, revue [À quelle fréquence utilisez-vous les médias suivants?]".
VARIABLE LABELS qa_f1_internet "Internet [À quelle fréquence utilisez-vous les médias suivants?]".
VARIABLE LABELS qf_at1 "Promenades à pied [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at3 "Jardinage [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at5 "Gymnastique, exercice physique [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at7 "Sport [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at11 "Aller au café, tea-room, restaurant [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at13 "Aller au cinéma, concert, théâtre [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at15 "Excursions de 1-2 jours en voiture, en car, train [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at17 "Voyages de 3-4 jours au moins (chez des parents, amis ou en vacances) [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at19 "Jouer d'un instrument de musique [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at21 "Autres activités artistiques [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at23 "Suivre à l'extérieur des cours, des conférences [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at25 "Jeux de société (cartes, Monopoly, Scrabble, loto, échecs, jeu de dames...) [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at27 "Faire des mots croisés, des patiences, des «mots cachés», des rébus, des sudoku, etc. [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at29 "Travaux d'aiguille (tricot, couture), poterie [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at31 "Autres travaux manuels (bricolage, réparations, menuiserie, mécanique, poterie) [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at33 "Assister à des manifestations politiques, syndicales [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at35 "Assister à des manifestations villageoises ou de quartier (fêtes, etc.) [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at37 "Assister à des manifestations sportives [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at39 "Prier [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at41 "Aller à l'église, au temple [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at43 "Maison de quartier / espace quartier [Pouvez-vous me dire à quelle fréquence vous pratiquez cette activité maintenant:]".
VARIABLE LABELS qf_at8_sport1 "Quel(s) sport(s) pratiquez-vous? Veuillez m'en citer deux au maximum. Sport 1 - texte".
VARIABLE LABELS qf_at8_sport2 "Quel(s) sport(s) pratiquez-vous? Veuillez m'en citer deux au maximum. Sport 2 - texte".

*Définition des labels de valeurs

VALUE LABELS canton
	1	"Genève"
	2	"Valais"
	3	"Bern - Mitteland"
	4	"Bern - Oberland"
	5	"Bern - Seeland"
	6	"Bâle"
	7	"Tessin"
	9	"Linked lives".
VALUE LABELS genre
	1	"Femme"
	2	"Homme".
VALUE LABELS age_entretien_groupe
	1	"65-69 ans"
	2	"70-74 ans"
	3	"75-79 ans"
	4	"80-84 ans"
	5	"85-89 ans"
	6	"90 ans et plus".
VALUE LABELS qp_a3
	-2	"NR".
VALUE LABELS qp_a4
	1	"Féminin"
	2	"Masculin".
VALUE LABELS qp_a5
	-2	"NR"
	1	"Conjoint(e), partenaire"
	2	"Enfant"
	3	"Frère ‎/ Sœur"
	4	"Autre membre de la famille"
	5	"Ami(e)"
	6	"Voisin(e)"
	7	"Employé(e), garde, infirmier(ère), autre professionnel".
VALUE LABELS qp_a6
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qp_a7
	-2	"NR"
	1	"Tous les jours ou presque"
	2	"Un jour sur deux"
	3	"Au moins une fois par semaine"
	4	"Au moins une fois tous les quinze jour"
	5	"Moins".
VALUE LABELS qp_a8
	-2	"NR"
	1	"Moins de 3 mois"
	2	"3 a 6 mois"
	3	"6 mois a 1 an"
	4	"Plus d'un an"
	5	"Plus de 3 ans"
	6	"Plus de 10 ans".
VALUE LABELS qp_a9
	-2	"NR"
	1	"Retraité(e)"
	2	"Exerce une profession"
	3	"Au chômage"
	4	"Au foyer"
	5	"Autre".
VALUE LABELS qp_a9_autre
	-2	"NR"
	-3	"NV"
	-7	"INAP".
VALUE LABELS qp_a10
	-2	"NR"
	1	"Dirigeants"
	2	"Professions libérales"
	3	"Autres indépendants"
	4	"Agriculteurs-exploitants"
	5	"Professions intellectuelles et d'encadrement"
	6	"Professions intermédiaires"
	7	"Non manuels qualifiés"
	8	"Manuels qualifiés"
	9	"Non manuels non qualifiés"
	10	"Manuels non qualifiés"
	11	"Non-actifs"
	12	"Autre".
VALUE LABELS qp_a10_autre
	-2	"NR"
	-3	"NV"
	-7	"INAP".
VALUE LABELS qp_a11
	-2	"NR".
VALUE LABELS qa_a2
	-2	"NR"
	8100	"Suisse"
	8201	"Albanie"
	8202	"Andorre"
	8204	"Belgique"
	8205	"Bulgarie"
	8206	"Danemark"
	8207	"Allemagne"
	8211	"Finlande"
	8212	"France"
	8214	"Grèce"
	8215	"Royaume-Uni"
	8216	"Irlande"
	8217	"Islande"
	8218	"Italie"
	8222	"Liechtenstein"
	8223	"Luxembourg"
	8224	"Malte"
	8226	"Monaco"
	8227	"Pays-Bas"
	8228	"Norvège"
	8229	"Autriche"
	8230	"Pologne"
	8231	"Portugal"
	8232	"Roumanie"
	8233	"Saint-Marin"
	8234	"Suède"
	8236	"Espagne"
	8238	"Tchécoslovaquie"
	8239	"Turquie"
	8240	"Hongrie"
	8241	"Cité du Vatican"
	8242	"Chypre"
	8243	"Slovaquie"
	8244	"République tchèque"
	8248	"Serbie"
	8249	"Yougoslavie"
	8250	"Croatie"
	8251	"Slovénie"
	8252	"Bosnie-Herzégovine"
	8255	"Macédoine"
	8256	"Kosovo"
	8260	"Estonie"
	8261	"Lettonie"
	8262	"Lituanie"
	8263	"Moldavie"
	8264	"Russie"
	8265	"Ukraine"
	8266	"Biélorussie"
	8301	"Guinée équatoriale"
	8302	"Éthiopie"
	8303	"Djibouti"
	8304	"Algérie"
	8305	"Angola"
	8307	"Botswana"
	8308	"Burundi"
	8309	"Bénin"
	8310	"Côte d'Ivoire"
	8311	"Gabon"
	8312	"Gambie"
	8313	"Ghana"
	8314	"Guinée-Bissau"
	8315	"Guinée"
	8317	"Cameroun"
	8319	"Cap-Vert"
	8320	"Kenya"
	8321	"Comores"
	8322	"République du Congo (Congo-Brazzaville)"
	8323	"République démocratique du Congo (Congo-Kinshasa)"
	8324	"Lesotho"
	8325	"Liberia"
	8326	"Libye"
	8327	"Madagascar"
	8329	"Malawi"
	8330	"Mali"
	8331	"Maroc"
	8332	"Mauritanie"
	8333	"Maurice"
	8334	"Mozambique"
	8335	"Niger"
	8336	"Nigeria"
	8337	"Burkina Faso"
	8340	"Zimbabwe"
	8341	"Rwanda"
	8343	"Zambie"
	8344	"Sao Tomé-et-Principe"
	8345	"Sénégal"
	8346	"Seychelles"
	8347	"Sierra Leone"
	8348	"Somalie"
	8349	"Afrique du Sud"
	8350	"Soudan"
	8351	"Namibie"
	8352	"Swaziland"
	8353	"Tanzanie"
	8354	"Togo"
	8356	"Tchad"
	8357	"Tunisie"
	8358	"Ouganda"
	8359	"Égypte"
	8360	"République centrafricaine"
	8362	"Érythrée"
	8372	"Sahara occidental"
	8401	"Argentine"
	8402	"Bahamas"
	8403	"Barbade"
	8405	"Bolivie"
	8406	"Brésil"
	8407	"Chili"
	8408	"Costa Rica"
	8409	"République dominicaine"
	8410	"Équateur"
	8411	"El Salvador"
	8415	"Guatemala"
	8417	"Guyana"
	8418	"Haïti"
	8419	"Belize"
	8420	"Honduras"
	8421	"Jamaïque"
	8423	"Canada"
	8424	"Colombie"
	8425	"Cuba"
	8427	"Mexique"
	8429	"Nicaragua"
	8430	"Panama"
	8431	"Paraguay"
	8432	"Pérou"
	8435	"Suriname"
	8436	"Trinité-et-Tobago"
	8437	"Uruguay"
	8438	"Venezuela"
	8439	"États-Unis"
	8440	"Dominique"
	8441	"Grenade"
	8442	"Antigua-et-Barbuda"
	8443	"Sainte-Lucie"
	8444	"Saint-Vincent-et-les-Grenadine"
	8445	"Saint-Kitts-et-Nevis"
	8501	"Afghanistan"
	8502	"Bahreïn"
	8503	"Bhoutan"
	8504	"Brunei"
	8505	"Myanmar"
	8506	"Sri Lanka"
	8507	"Taïwan"
	8508	"Chine"
	8510	"Inde"
	8511	"Indonésie"
	8512	"Irak"
	8513	"Iran"
	8514	"Israël"
	8515	"Japon"
	8516	"Yémen"
	8517	"Jordanie"
	8518	"Cambodge"
	8519	"Qatar"
	8521	"Koweït"
	8522	"Laos"
	8523	"Liban"
	8525	"Malaisie"
	8526	"Maldives"
	8527	"Oman"
	8528	"Mongolie"
	8529	"Népal"
	8530	"Corée du nord"
	8532	"Émirats arabes unis"
	8533	"Pakistan"
	8534	"Philippines"
	8535	"Arabie saoudite"
	8537	"Singapour"
	8539	"Corée du sud"
	8541	"Syrie"
	8542	"Thaïlande"
	8545	"Vietnam"
	8546	"Bangladesh"
	8550	"Palestine"
	8560	"Arménie"
	8561	"Azerbaïdjan"
	8562	"Géorgie"
	8563	"Kazakhstan"
	8564	"Kirghizistan"
	8565	"Tadjikistan"
	8566	"Turkménistan"
	8567	"Ouzbékistan"
	8601	"Australie"
	8602	"Fidji"
	8604	"Nauru"
	8605	"Vanuatu"
	8607	"Nouvelle-Zélande"
	8608	"Papouasie-Nouvelle-Guinée"
	8610	"Tonga"
	8612	"Samoa"
	8614	"Iles Salomon"
	8615	"Tuvalu"
	8616	"Kiribati"
	8617	"Iles Marshall"
	8618	"Micronésie"
	8619	"Palau"
	9998	"Apatride".
VALUE LABELS qa_a4
	-2	"NR"
	8100	"Suisse"
	8201	"Albanie"
	8202	"Andorre"
	8204	"Belgique"
	8205	"Bulgarie"
	8206	"Danemark"
	8207	"Allemagne"
	8211	"Finlande"
	8212	"France"
	8214	"Grèce"
	8215	"Royaume-Uni"
	8216	"Irlande"
	8217	"Islande"
	8218	"Italie"
	8222	"Liechtenstein"
	8223	"Luxembourg"
	8224	"Malte"
	8226	"Monaco"
	8227	"Pays-Bas"
	8228	"Norvège"
	8229	"Autriche"
	8230	"Pologne"
	8231	"Portugal"
	8232	"Roumanie"
	8233	"Saint-Marin"
	8234	"Suède"
	8236	"Espagne"
	8238	"Tchécoslovaquie"
	8239	"Turquie"
	8240	"Hongrie"
	8241	"Cité du Vatican"
	8242	"Chypre"
	8243	"Slovaquie"
	8244	"République tchèque"
	8248	"Serbie"
	8249	"Yougoslavie"
	8250	"Croatie"
	8251	"Slovénie"
	8252	"Bosnie-Herzégovine"
	8255	"Macédoine"
	8256	"Kosovo"
	8260	"Estonie"
	8261	"Lettonie"
	8262	"Lituanie"
	8263	"Moldavie"
	8264	"Russie"
	8265	"Ukraine"
	8266	"Biélorussie"
	8301	"Guinée équatoriale"
	8302	"Éthiopie"
	8303	"Djibouti"
	8304	"Algérie"
	8305	"Angola"
	8307	"Botswana"
	8308	"Burundi"
	8309	"Bénin"
	8310	"Côte d'Ivoire"
	8311	"Gabon"
	8312	"Gambie"
	8313	"Ghana"
	8314	"Guinée-Bissau"
	8315	"Guinée"
	8317	"Cameroun"
	8319	"Cap-Vert"
	8320	"Kenya"
	8321	"Comores"
	8322	"République du Congo (Congo-Brazzaville)"
	8323	"République démocratique du Congo (Congo-Kinshasa)"
	8324	"Lesotho"
	8325	"Liberia"
	8326	"Libye"
	8327	"Madagascar"
	8329	"Malawi"
	8330	"Mali"
	8331	"Maroc"
	8332	"Mauritanie"
	8333	"Maurice"
	8334	"Mozambique"
	8335	"Niger"
	8336	"Nigeria"
	8337	"Burkina Faso"
	8340	"Zimbabwe"
	8341	"Rwanda"
	8343	"Zambie"
	8344	"Sao Tomé-et-Principe"
	8345	"Sénégal"
	8346	"Seychelles"
	8347	"Sierra Leone"
	8348	"Somalie"
	8349	"Afrique du Sud"
	8350	"Soudan"
	8351	"Namibie"
	8352	"Swaziland"
	8353	"Tanzanie"
	8354	"Togo"
	8356	"Tchad"
	8357	"Tunisie"
	8358	"Ouganda"
	8359	"Égypte"
	8360	"République centrafricaine"
	8362	"Érythrée"
	8372	"Sahara occidental"
	8401	"Argentine"
	8402	"Bahamas"
	8403	"Barbade"
	8405	"Bolivie"
	8406	"Brésil"
	8407	"Chili"
	8408	"Costa Rica"
	8409	"République dominicaine"
	8410	"Équateur"
	8411	"El Salvador"
	8415	"Guatemala"
	8417	"Guyana"
	8418	"Haïti"
	8419	"Belize"
	8420	"Honduras"
	8421	"Jamaïque"
	8423	"Canada"
	8424	"Colombie"
	8425	"Cuba"
	8427	"Mexique"
	8429	"Nicaragua"
	8430	"Panama"
	8431	"Paraguay"
	8432	"Pérou"
	8435	"Suriname"
	8436	"Trinité-et-Tobago"
	8437	"Uruguay"
	8438	"Venezuela"
	8439	"États-Unis"
	8440	"Dominique"
	8441	"Grenade"
	8442	"Antigua-et-Barbuda"
	8443	"Sainte-Lucie"
	8444	"Saint-Vincent-et-les-Grenadine"
	8445	"Saint-Kitts-et-Nevis"
	8501	"Afghanistan"
	8502	"Bahreïn"
	8503	"Bhoutan"
	8504	"Brunei"
	8505	"Myanmar"
	8506	"Sri Lanka"
	8507	"Taïwan"
	8508	"Chine"
	8510	"Inde"
	8511	"Indonésie"
	8512	"Irak"
	8513	"Iran"
	8514	"Israël"
	8515	"Japon"
	8516	"Yémen"
	8517	"Jordanie"
	8518	"Cambodge"
	8519	"Qatar"
	8521	"Koweït"
	8522	"Laos"
	8523	"Liban"
	8525	"Malaisie"
	8526	"Maldives"
	8527	"Oman"
	8528	"Mongolie"
	8529	"Népal"
	8530	"Corée du nord"
	8532	"Émirats arabes unis"
	8533	"Pakistan"
	8534	"Philippines"
	8535	"Arabie saoudite"
	8537	"Singapour"
	8539	"Corée du sud"
	8541	"Syrie"
	8542	"Thaïlande"
	8545	"Vietnam"
	8546	"Bangladesh"
	8550	"Palestine"
	8560	"Arménie"
	8561	"Azerbaïdjan"
	8562	"Géorgie"
	8563	"Kazakhstan"
	8564	"Kirghizistan"
	8565	"Tadjikistan"
	8566	"Turkménistan"
	8567	"Ouzbékistan"
	8601	"Australie"
	8602	"Fidji"
	8604	"Nauru"
	8605	"Vanuatu"
	8607	"Nouvelle-Zélande"
	8608	"Papouasie-Nouvelle-Guinée"
	8610	"Tonga"
	8612	"Samoa"
	8614	"Iles Salomon"
	8615	"Tuvalu"
	8616	"Kiribati"
	8617	"Iles Marshall"
	8618	"Micronésie"
	8619	"Palau"
	9998	"Apatride".
VALUE LABELS qf_y3
	-2	"NR"
	-1	"NSP"
	1	"Permis B (autorisation de séjour)"
	2	"Permis C (autorisation d'établissement)"
	3	"Permis F (admission provisoire)"
	4	"Permis N (requérant d'asile)"
	5	"Permis L (permis de séjour de courte durée)".
VALUE LABELS qa_a5_francais
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_allemand
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_italien
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_anglais
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_espagnol
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_portugais
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_turc
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_sercrobos
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_albanais
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a5_autre
	0	"Non"
	1	"Oui".
VALUE LABELS qa_a7
	-2	"NR"
	1	"Célibataire"
	2	"Marié(e), remarié(e), pacsé(e)"
	3	"Divorcé(e), séparé(e)"
	4	"Veuf (veuve)".
VALUE LABELS qa_a8
	-2	"NR"
	-1	"NSP"
	1	"Primaire"
	2	"Secondaire inférieur (cycle d'orientation)"
	3	"Formation professionnelle (apprentissage)"
	4	"Secondaire supérieur (collège, école de commerce, gymnase, etc.)"
	5	"École technique ou professionnelle supérieure (ex: infirmières, assistants sociaux, maîtres d'école, ingénieur etc.)"
	6	"Université, école polytechnique fédérale"
	7	"Autre".
VALUE LABELS qa_m1
	-3	"NV"
	-2	"NR"
	-1	"NSP"
	0	"Sans confession"
	1	"Catholique"
	2	"Protestante"
	3	"Israélite"
	4	"Musulmane"
	5	"Témoin de Jehovah"
	6	"Orthodoxe"
	7	"Apostolique"
	8	"Évangelique"
	9	"Bouddhiste"
	10	"Anglicane"
	11	"Autre".
VALUE LABELS qa_c1
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qa_c2
	-7	"INAP"
	-3	"NV"
	-2	"NR".
VALUE LABELS qa_c3
	-7	"INAP"
	-3	"NV"
	-2	"NR".
VALUE LABELS qa_c4
	-7	"INAP"
	-3	"NV"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_t1
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_t2
	-7	"INAP"
	-2	"NR".
VALUE LABELS qf_t3
	-7	"INAP"
	-2	"NR".
VALUE LABELS qa_e1
	-3	"NV"
	-2	"NR"
	1	"Chez vous (propriétaire)"
	2	"Chez vous (locataire d'une maison ou d'un appartement privé)"
	3	"Chez vous dans un appartement protégé (immeuble avec encadrement médico-social, DOMINO, etc.)"
	4	"En établissement médico-social (EMS)"
	5	"Chez un de vos enfants"
	6	"Chez un autre membre de votre famille"
	7	"Chez un(e) ami(e)"
	8	"Dans un couvent"
	9	"Autre".
VALUE LABELS qa_e5
	-7	"INAP"
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qa_e6
	-7	"INAP"
	-2	"NR".
VALUE LABELS qa_e7
	-7	"INAP"
	-3	"NV"
	-2	"NR"
	1	"Conjoint(e), partenaire"
	2	"Enfant(s)"
	3	"Parent(s), beaux-parent(s)"
	4	"Autre(s) membre(s) de la famille"
	5	"Ami(s), amie(s)"
	6	"Employé(s), employée(s)".
VALUE LABELS qa_e7b
	-7	"INAP"
	"-4"	"NV"
	-3	"NV"
	-2	"NR"
	1	"Conjoint(e), partenaire"
	2	"Enfant(s)"
	3	"Parent(s), beaux-parent(s)"
	4	"Autre(s) membre(s) de la famille"
	5	"Ami(s), amie(s)"
	6	"Employé(s), employée(s)".
VALUE LABELS qa_e7c
	-7	"INAP"
	-3	"NV"
	-2	"NR"
	1	"Conjoint(e), partenaire"
	2	"Enfant(s)"
	3	"Parent(s), beaux-parent(s)"
	4	"Autre(s) membre(s) de la famille"
	5	"Ami(s), amie(s)"
	6	"Employé(s), employée(s)".
VALUE LABELS qa_e9
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_x1
	-2	"NR"
	-1	"NSP"
	1	"Dirigeants"
	2	"Professions libérales"
	3	"Autres indépendants"
	4	"Agriculteurs-exploitants"
	5	"Professions intellectuelles et d'encadrement"
	6	"Professions intermédiaires"
	7	"Non manuels qualifiés"
	8	"Manuels qualifiés"
	9	"Non manuels non qualifiés"
	10	"Manuels non qualifiés"
	11	"Non-actifs".
VALUE LABELS qf_x1_autre
	-2	"NR"
	-3	"NV".
VALUE LABELS qf_x2
	-7	"INAP"
	-2	"NR".
VALUE LABELS qf_x3
	-2	"NR"
	-1	"NSP"
	1	"Dirigeants"
	2	"Professions libérales"
	3	"Autres indépendants"
	4	"Agriculteurs-exploitants"
	5	"Professions intellectuelles et d'encadrement"
	6	"Professions intermédiaires"
	7	"Non manuels qualifiés"
	8	"Manuels qualifiés"
	9	"Non manuels non qualifiés"
	10	"Manuels non qualifiés"
	11	"Non-actifs".
VALUE LABELS qf_x3_autre
	-2	"NR"
	-3	"NV".
VALUE LABELS qf_x4
	-7	"INAP"
	-2	"NR".
VALUE LABELS qf_ar2
	-2	"NR"
	1	"Moins de 1200 Frs"
	2	"De 1200 à moins de 2400 Frs"
	3	"De 2400 à moins de 3600 Frs"
	4	"De 3600 à moins de 4800 Frs"
	5	"De 4800 à moins de 6000 Frs"
	6	"De 6000 à moins de 7200 Frs"
	7	"De 7200 à moins de 10'000 Frs"
	8	"De 10'000 à moins de 15'000 Frs"
	9	"Plus de 15'000 Frs".
VALUE LABELS qa_i1
	-2	"NR"
	-1	"NSP".
VALUE LABELS qa_i2
	-2	"NR"
	-1	"NSP".
VALUE LABELS qf_z1_maladie
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_z1_accident
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_z1_operation
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_z8
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_z9
	-7	"INAP"
	-2	"NR".
VALUE LABELS qf_ac4
	-2	"NR"
	0	"Non"
	1	"Oui".
VALUE LABELS qf_ac5
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qa_i8_membreinferieur
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_membresuperieur
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_tete
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_dos
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_coeur
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_respiratoire
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_ventre
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_vessiegenital
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_poitrine
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_fievre
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i8_autre
	-3	"NV"
	-2	"NR"
	0	"Non, pas du tout"
	1	"Oui, un peu"
	2	"Oui, beaucoup".
VALUE LABELS qa_i11
	-3	"NV"
	-2	"NR"
	0	"Non"
	1	"Oui, mais avec difficulté"
	2	"Oui, sans difficulté majeure".
VALUE LABELS qa_i13
	-3	"NV"
	-2	"NR"
	0	"Non"
	1	"Oui, mais avec difficulté"
	2	"Oui, sans difficulté majeure".
VALUE LABELS qa_i14
	-3	"NV"
	-2	"NR"
	0	"Non"
	1	"Oui, mais avec difficulté"
	2	"Oui, sans difficulté majeure".
VALUE LABELS qa_i15
	-2	"NR"
	-1	"NV"
	0	"Non"
	1	"Oui".
VALUE LABELS qa_i16
	-7	"INAP"
	-2	"NR".
VALUE LABELS qf_ac1_appetit
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_isole
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_dormir
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_plaisir
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_temps_long
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_triste
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_confiance
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_pleurer
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_fatigue
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_sante
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_irritable
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_confiance_avenir
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qf_ac1_anxieux
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Souvent"
	3	"Toujours".
VALUE LABELS qa_i19_deplacer
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qa_i19_escalier
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qa_i19_exterieur
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qa_i19_marcher200
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qa_i19_coucherlever
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qa_i19_habiller
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qa_i19_manger
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qa_i19_laver
	-2	"NR"
	0	"Ne peut pas le faire seul(e)"
	1	"Seul(e), mais avec difficulté"
	2	"Seul(e), sans difficulté majeure".
VALUE LABELS qf_ac3_continence
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_telephoner
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_courses
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_repas
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_menage
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_lessive
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_transports
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_medicaments
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_ac3_finances
	-2	"NR"
	1	"Indépendant"
	2	"Avec aide"
	3	"Dépendant".
VALUE LABELS qf_aq3_menage
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_repas
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_courses
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_reparations
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_remplir_impot
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_aider_toilette
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_presence
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_emmener_promenade
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq3_aide_financiere
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_menage
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_repas
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_courses
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_reparations
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_remplir_impot
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_aider_toilette
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_presence
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qf_aq7_emmener_personne
	-2	"NR"
	0	"Jamais"
	1	"Rarement"
	2	"Parfois"
	3	"Souvent".
VALUE LABELS qa_j1_infirmiere
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_service_aide
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_physiotherapeute
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_assistant_social
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_repas
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_hopital_jour
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_benevole
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_employe_menage
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j1_autre
	-2	"NR"
	0	"Jamais"
	1	"Moins souvent"
	2	"Une fois pas mois"
	3	"Tous les 15 jours"
	4	"Environ une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS qa_j2
	-2	"NR"
	-3	"NV".
VALUE LABELS qa_l1_chezvous
	-2	"NR"
	0	"Non, ou presque jamais"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qa_l1_chezeux
	-2	"NR"
	0	"Non, ou presque jamais"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qa_l1_telephone
	-2	"NR"
	0	"Non, ou presque jamais"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qa_l2_chezvous
	-2	"NR"
	0	"Non, ou presque jamais"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qa_l2_chezeux
	-2	"NR"
	0	"Non, ou presque jamais"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qa_l2_telephone
	-2	"NR"
	0	"Non, ou presque jamais"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qa_f1_radio
	-2	"NR"
	0	"Jamais"
	1	"Rarement, moins d'une fois par semaine"
	2	"Une ou quelques fois par semaine"
	3	"Tous les jours, moins de 3 heures"
	4	"Tous les jours, plus de 3 heures".
VALUE LABELS qa_f1_television
	-2	"NR"
	0	"Jamais"
	1	"Rarement, moins d'une fois par semaine"
	2	"Une ou quelques fois par semaine"
	3	"Tous les jours, moins de 3 heures"
	4	"Tous les jours, plus de 3 heures".
VALUE LABELS qa_f1_journal
	-2	"NR"
	0	"Jamais"
	1	"Rarement, moins d'une fois par semaine"
	2	"Une ou quelques fois par semaine"
	3	"Tous les jours, moins de 3 heures"
	4	"Tous les jours, plus de 3 heures".
VALUE LABELS qa_f1_livre
	-2	"NR"
	0	"Jamais"
	1	"Rarement, moins d'une fois par semaine"
	2	"Une ou quelques fois par semaine"
	3	"Tous les jours, moins de 3 heures"
	4	"Tous les jours, plus de 3 heures".
VALUE LABELS qa_f1_internet
	-2	"NR"
	0	"Jamais"
	1	"Rarement, moins d'une fois par semaine"
	2	"Une ou quelques fois par semaine"
	3	"Tous les jours, moins de 3 heures"
	4	"Tous les jours, plus de 3 heures".
VALUE LABELS qf_at1
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at3
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at5
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at7
	-3	"NV"
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at11
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at13
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at15
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at17
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at19
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at21
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at23
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at25
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at27
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at29
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at31
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at33
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at35
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at37
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at39
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at41
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at43
	-2	"NR"
	0	"Non"
	1	"Oui, au moins une fois par an"
	2	"Oui, au moins une fois par mois"
	3	"Oui, au moins une fois par semaine"
	4	"Oui, tous les jours ou presque".
VALUE LABELS qf_at8_sport1
	-2	"NR"
	-3	"NV".
VALUE LABELS qf_at8_sport2
	-2	"NR"
	-3	"NV".

* Définition de la mesure des variables

VARIABLE LEVEL id(NOMINAL).
VARIABLE LEVEL code_ego(NOMINAL).
VARIABLE LEVEL code_enqueteur(NOMINAL).
VARIABLE LEVEL canton(NOMINAL).
VARIABLE LEVEL genre(NOMINAL).
VARIABLE LEVEL age_entretien2(SCALE).
VARIABLE LEVEL age_entretien_groupe(ORDINAL).
VARIABLE LEVEL qp_a3(SCALE).
VARIABLE LEVEL qp_a4(NOMINAL).
VARIABLE LEVEL qp_a5(NOMINAL).
VARIABLE LEVEL qp_a6(NOMINAL).
VARIABLE LEVEL qp_a7(ORDINAL).
VARIABLE LEVEL qp_a8(ORDINAL).
VARIABLE LEVEL qp_a9(NOMINAL).
VARIABLE LEVEL qp_a9_autre(NOMINAL).
VARIABLE LEVEL qp_a10(NOMINAL).
VARIABLE LEVEL qp_a10_autre(NOMINAL).
VARIABLE LEVEL qp_a11(SCALE).
VARIABLE LEVEL qa_a1(ORDINAL).
VARIABLE LEVEL qa_a2(NOMINAL).
VARIABLE LEVEL qa_a4(NOMINAL).
VARIABLE LEVEL qf_y3(NOMINAL).
VARIABLE LEVEL qa_a5_francais(NOMINAL).
VARIABLE LEVEL qa_a5_allemand(NOMINAL).
VARIABLE LEVEL qa_a5_italien(NOMINAL).
VARIABLE LEVEL qa_a5_anglais(NOMINAL).
VARIABLE LEVEL qa_a5_espagnol(NOMINAL).
VARIABLE LEVEL qa_a5_portugais(NOMINAL).
VARIABLE LEVEL qa_a5_turc(NOMINAL).
VARIABLE LEVEL qa_a5_sercrobos(NOMINAL).
VARIABLE LEVEL qa_a5_albanais(NOMINAL).
VARIABLE LEVEL qa_a5_autre(NOMINAL).
VARIABLE LEVEL qa_a6(NOMINAL).
VARIABLE LEVEL qa_a7(NOMINAL).
VARIABLE LEVEL qa_a8(ORDINAL).
VARIABLE LEVEL qa_a8_autre_txt(NOMINAL).
VARIABLE LEVEL qa_m1(NOMINAL).
VARIABLE LEVEL qa_m2(NOMINAL).
VARIABLE LEVEL qa_c1(NOMINAL).
VARIABLE LEVEL qa_c2(SCALE).
VARIABLE LEVEL qa_c3(SCALE).
VARIABLE LEVEL qa_c4(NOMINAL).
VARIABLE LEVEL qf_t1(NOMINAL).
VARIABLE LEVEL qf_t2(SCALE).
VARIABLE LEVEL qf_t3(SCALE).
VARIABLE LEVEL qa_e1(NOMINAL).
VARIABLE LEVEL qa_e2(NOMINAL).
VARIABLE LEVEL qa_e5(NOMINAL).
VARIABLE LEVEL qa_e6(SCALE).
VARIABLE LEVEL qa_e7(NOMINAL).
VARIABLE LEVEL qa_e7b(NOMINAL).
VARIABLE LEVEL qa_e7c(NOMINAL).
VARIABLE LEVEL qa_e8(NOMINAL).
VARIABLE LEVEL qa_e9(NOMINAL).
VARIABLE LEVEL qf_x1(NOMINAL).
VARIABLE LEVEL qf_x1_autre(NOMINAL).
VARIABLE LEVEL qf_x2(SCALE).
VARIABLE LEVEL qf_x3(NOMINAL).
VARIABLE LEVEL qf_x3_autre(NOMINAL).
VARIABLE LEVEL qf_x4(SCALE).
VARIABLE LEVEL qf_ar2(ORDINAL).
VARIABLE LEVEL qa_i1(SCALE).
VARIABLE LEVEL qa_i2(SCALE).
VARIABLE LEVEL qf_z1_maladie(NOMINAL).
VARIABLE LEVEL qf_z1_accident(NOMINAL).
VARIABLE LEVEL qf_z1_operation(NOMINAL).
VARIABLE LEVEL qf_z8(NOMINAL).
VARIABLE LEVEL qf_z9(SCALE).
VARIABLE LEVEL qf_ac4(NOMINAL).
VARIABLE LEVEL qf_ac5(ORDINAL).
VARIABLE LEVEL qa_i8_membreinferieur(ORDINAL).
VARIABLE LEVEL qa_i8_membresuperieur(ORDINAL).
VARIABLE LEVEL qa_i8_tete(ORDINAL).
VARIABLE LEVEL qa_i8_dos(ORDINAL).
VARIABLE LEVEL qa_i8_coeur(ORDINAL).
VARIABLE LEVEL qa_i8_respiratoire(ORDINAL).
VARIABLE LEVEL qa_i8_ventre(ORDINAL).
VARIABLE LEVEL qa_i8_vessiegenital(ORDINAL).
VARIABLE LEVEL qa_i8_poitrine(ORDINAL).
VARIABLE LEVEL qa_i8_fievre(ORDINAL).
VARIABLE LEVEL qa_i8_autre(ORDINAL).
VARIABLE LEVEL qa_i9_autre_txt(NOMINAL).
VARIABLE LEVEL qa_i11(ORDINAL).
VARIABLE LEVEL qa_i13(ORDINAL).
VARIABLE LEVEL qa_i14(ORDINAL).
VARIABLE LEVEL qa_i15(NOMINAL).
VARIABLE LEVEL qa_i16(SCALE).
VARIABLE LEVEL qf_ac1_appetit(ORDINAL).
VARIABLE LEVEL qf_ac1_isole(ORDINAL).
VARIABLE LEVEL qf_ac1_dormir(ORDINAL).
VARIABLE LEVEL qf_ac1_plaisir(ORDINAL).
VARIABLE LEVEL qf_ac1_temps_long(ORDINAL).
VARIABLE LEVEL qf_ac1_triste(ORDINAL).
VARIABLE LEVEL qf_ac1_confiance(ORDINAL).
VARIABLE LEVEL qf_ac1_pleurer(ORDINAL).
VARIABLE LEVEL qf_ac1_fatigue(ORDINAL).
VARIABLE LEVEL qf_ac1_sante(ORDINAL).
VARIABLE LEVEL qf_ac1_irritable(ORDINAL).
VARIABLE LEVEL qf_ac1_confiance_avenir(ORDINAL).
VARIABLE LEVEL qf_ac1_anxieux(ORDINAL).
VARIABLE LEVEL qa_i19_deplacer(ORDINAL).
VARIABLE LEVEL qa_i19_escalier(ORDINAL).
VARIABLE LEVEL qa_i19_exterieur(ORDINAL).
VARIABLE LEVEL qa_i19_marcher200(ORDINAL).
VARIABLE LEVEL qa_i19_coucherlever(ORDINAL).
VARIABLE LEVEL qa_i19_habiller(ORDINAL).
VARIABLE LEVEL qa_i19_manger(ORDINAL).
VARIABLE LEVEL qa_i19_laver(ORDINAL).
VARIABLE LEVEL qf_ac3_continence(ORDINAL).
VARIABLE LEVEL qf_ac3_telephoner(ORDINAL).
VARIABLE LEVEL qf_ac3_courses(NOMINAL).
VARIABLE LEVEL qf_ac3_repas(ORDINAL).
VARIABLE LEVEL qf_ac3_menage(ORDINAL).
VARIABLE LEVEL qf_ac3_lessive(ORDINAL).
VARIABLE LEVEL qf_ac3_transports(ORDINAL).
VARIABLE LEVEL qf_ac3_medicaments(ORDINAL).
VARIABLE LEVEL qf_ac3_finances(ORDINAL).
VARIABLE LEVEL qf_aq3_menage(ORDINAL).
VARIABLE LEVEL qf_aq3_repas(ORDINAL).
VARIABLE LEVEL qf_aq3_courses(ORDINAL).
VARIABLE LEVEL qf_aq3_reparations(ORDINAL).
VARIABLE LEVEL qf_aq3_remplir_impot(ORDINAL).
VARIABLE LEVEL qf_aq3_aider_toilette(ORDINAL).
VARIABLE LEVEL qf_aq3_presence(ORDINAL).
VARIABLE LEVEL qf_aq3_emmener_promenade(ORDINAL).
VARIABLE LEVEL qf_aq3_aide_financiere(ORDINAL).
VARIABLE LEVEL qf_aq7_menage(ORDINAL).
VARIABLE LEVEL qf_aq7_repas(ORDINAL).
VARIABLE LEVEL qf_aq7_courses(ORDINAL).
VARIABLE LEVEL qf_aq7_reparations(ORDINAL).
VARIABLE LEVEL qf_aq7_remplir_impot(ORDINAL).
VARIABLE LEVEL qf_aq7_aider_toilette(ORDINAL).
VARIABLE LEVEL qf_aq7_presence(ORDINAL).
VARIABLE LEVEL qf_aq7_emmener_personne(ORDINAL).
VARIABLE LEVEL qa_j1_infirmiere(ORDINAL).
VARIABLE LEVEL qa_j1_service_aide(ORDINAL).
VARIABLE LEVEL qa_j1_physiotherapeute(ORDINAL).
VARIABLE LEVEL qa_j1_assistant_social(ORDINAL).
VARIABLE LEVEL qa_j1_repas(ORDINAL).
VARIABLE LEVEL qa_j1_hopital_jour(ORDINAL).
VARIABLE LEVEL qa_j1_benevole(ORDINAL).
VARIABLE LEVEL qa_j1_employe_menage(ORDINAL).
VARIABLE LEVEL qa_j1_autre(ORDINAL).
VARIABLE LEVEL qa_j2(NOMINAL).
VARIABLE LEVEL qa_l1_chezvous(ORDINAL).
VARIABLE LEVEL qa_l1_chezeux(ORDINAL).
VARIABLE LEVEL qa_l1_telephone(ORDINAL).
VARIABLE LEVEL qa_l2_chezvous(ORDINAL).
VARIABLE LEVEL qa_l2_chezeux(ORDINAL).
VARIABLE LEVEL qa_l2_telephone(ORDINAL).
VARIABLE LEVEL qa_f1_radio(ORDINAL).
VARIABLE LEVEL qa_f1_television(ORDINAL).
VARIABLE LEVEL qa_f1_journal(ORDINAL).
VARIABLE LEVEL qa_f1_livre(ORDINAL).
VARIABLE LEVEL qa_f1_internet(ORDINAL).
VARIABLE LEVEL qf_at1(ORDINAL).
VARIABLE LEVEL qf_at3(ORDINAL).
VARIABLE LEVEL qf_at5(ORDINAL).
VARIABLE LEVEL qf_at7(ORDINAL).
VARIABLE LEVEL qf_at11(ORDINAL).
VARIABLE LEVEL qf_at13(ORDINAL).
VARIABLE LEVEL qf_at15(ORDINAL).
VARIABLE LEVEL qf_at17(ORDINAL).
VARIABLE LEVEL qf_at19(ORDINAL).
VARIABLE LEVEL qf_at21(ORDINAL).
VARIABLE LEVEL qf_at23(ORDINAL).
VARIABLE LEVEL qf_at25(ORDINAL).
VARIABLE LEVEL qf_at27(ORDINAL).
VARIABLE LEVEL qf_at29(ORDINAL).
VARIABLE LEVEL qf_at31(ORDINAL).
VARIABLE LEVEL qf_at33(ORDINAL).
VARIABLE LEVEL qf_at35(ORDINAL).
VARIABLE LEVEL qf_at37(ORDINAL).
VARIABLE LEVEL qf_at39(ORDINAL).
VARIABLE LEVEL qf_at41(ORDINAL).
VARIABLE LEVEL qf_at43(ORDINAL).
VARIABLE LEVEL qf_at8_sport1(NOMINAL).
VARIABLE LEVEL qf_at8_sport2(NOMINAL).

* Définition des missings

MISSING VALUES qp_a3 (LOWEST THRU -1).
MISSING VALUES qp_a5 (LOWEST THRU -1).
MISSING VALUES qp_a6 (LOWEST THRU -1).
MISSING VALUES qp_a7 (LOWEST THRU -1).
MISSING VALUES qp_a8 (LOWEST THRU -1).
MISSING VALUES qp_a9 (LOWEST THRU -1).
MISSING VALUES qp_a9_autre ("-7", "-3", "-2").
MISSING VALUES qp_a10 (LOWEST THRU -1).
MISSING VALUES qp_a10_autre ("-7", "-3", "-2").
MISSING VALUES qp_a11 (LOWEST THRU -1).
MISSING VALUES qa_a2 (LOWEST THRU -1).
MISSING VALUES qa_a4 (LOWEST THRU -1).
MISSING VALUES qf_y3 (LOWEST THRU -1).
MISSING VALUES qa_a5_francais (LOWEST THRU -1).
MISSING VALUES qa_a5_allemand (LOWEST THRU -1).
MISSING VALUES qa_a5_italien (LOWEST THRU -1).
MISSING VALUES qa_a5_anglais (LOWEST THRU -1).
MISSING VALUES qa_a5_espagnol (LOWEST THRU -1).
MISSING VALUES qa_a5_portugais (LOWEST THRU -1).
MISSING VALUES qa_a5_turc (LOWEST THRU -1).
MISSING VALUES qa_a5_sercrobos (LOWEST THRU -1).
MISSING VALUES qa_a5_albanais (LOWEST THRU -1).
MISSING VALUES qa_a5_autre (LOWEST THRU -1).
MISSING VALUES qa_a6 ("-7", "-3", "-2").
MISSING VALUES qa_a7 (LOWEST THRU -1).
MISSING VALUES qa_a8 (LOWEST THRU -1).
MISSING VALUES qa_a8_autre_txt ("-7", "-3", "-2").
MISSING VALUES qa_m1 (LOWEST THRU -1).
MISSING VALUES qa_m2 ("-7", "-3", "-2").
MISSING VALUES qa_c1 (LOWEST THRU -1).
MISSING VALUES qa_c2 (LOWEST THRU -1).
MISSING VALUES qa_c3 (LOWEST THRU -1).
MISSING VALUES qa_c4 (LOWEST THRU -1).
MISSING VALUES qf_t1 (LOWEST THRU -1).
MISSING VALUES qf_t2 (LOWEST THRU -1).
MISSING VALUES qf_t3 (LOWEST THRU -1).
MISSING VALUES qa_e1 (LOWEST THRU -1).
MISSING VALUES qa_e2 ("-7", "-3", "-2").
MISSING VALUES qa_e5 (LOWEST THRU -1).
MISSING VALUES qa_e6 (LOWEST THRU -1).
MISSING VALUES qa_e7 (LOWEST THRU -1).
MISSING VALUES qa_e7b (LOWEST THRU -1).
MISSING VALUES qa_e7c (LOWEST THRU -1).
MISSING VALUES qa_e8 ("-7", "-3", "-2").
MISSING VALUES qa_e9 (LOWEST THRU -1).
MISSING VALUES qf_x1 (LOWEST THRU -1).
MISSING VALUES qf_x1_autre ("-7", "-3", "-2").
MISSING VALUES qf_x2 (LOWEST THRU -1).
MISSING VALUES qf_x3 (LOWEST THRU -1).
MISSING VALUES qf_x3_autre ("-7", "-3", "-2").
MISSING VALUES qf_x4 (LOWEST THRU -1).
MISSING VALUES qf_ar2 (LOWEST THRU -1).
MISSING VALUES qa_i1 (LOWEST THRU -1).
MISSING VALUES qa_i2 (LOWEST THRU -1).
MISSING VALUES qf_z1_maladie (LOWEST THRU -1).
MISSING VALUES qf_z1_accident (LOWEST THRU -1).
MISSING VALUES qf_z1_operation (LOWEST THRU -1).
MISSING VALUES qf_z8 (LOWEST THRU -1).
MISSING VALUES qf_z9 (LOWEST THRU -1).
MISSING VALUES qf_ac4 (LOWEST THRU -1).
MISSING VALUES qf_ac5 (LOWEST THRU -1).
MISSING VALUES qa_i8_membreinferieur (LOWEST THRU -1).
MISSING VALUES qa_i8_membresuperieur (LOWEST THRU -1).
MISSING VALUES qa_i8_tete (LOWEST THRU -1).
MISSING VALUES qa_i8_dos (LOWEST THRU -1).
MISSING VALUES qa_i8_coeur (LOWEST THRU -1).
MISSING VALUES qa_i8_respiratoire (LOWEST THRU -1).
MISSING VALUES qa_i8_ventre (LOWEST THRU -1).
MISSING VALUES qa_i8_vessiegenital (LOWEST THRU -1).
MISSING VALUES qa_i8_poitrine (LOWEST THRU -1).
MISSING VALUES qa_i8_fievre (LOWEST THRU -1).
MISSING VALUES qa_i8_autre (LOWEST THRU -1).
MISSING VALUES qa_i9_autre_txt ("-7", "-3", "-2").
MISSING VALUES qa_i11 (LOWEST THRU -1).
MISSING VALUES qa_i13 (LOWEST THRU -1).
MISSING VALUES qa_i14 (LOWEST THRU -1).
MISSING VALUES qa_i15 (LOWEST THRU -1).
MISSING VALUES qa_i16 (LOWEST THRU -1).
MISSING VALUES qf_ac1_appetit (LOWEST THRU -1).
MISSING VALUES qf_ac1_isole (LOWEST THRU -1).
MISSING VALUES qf_ac1_dormir (LOWEST THRU -1).
MISSING VALUES qf_ac1_plaisir (LOWEST THRU -1).
MISSING VALUES qf_ac1_temps_long (LOWEST THRU -1).
MISSING VALUES qf_ac1_triste (LOWEST THRU -1).
MISSING VALUES qf_ac1_confiance (LOWEST THRU -1).
MISSING VALUES qf_ac1_pleurer (LOWEST THRU -1).
MISSING VALUES qf_ac1_fatigue (LOWEST THRU -1).
MISSING VALUES qf_ac1_sante (LOWEST THRU -1).
MISSING VALUES qf_ac1_irritable (LOWEST THRU -1).
MISSING VALUES qf_ac1_confiance_avenir (LOWEST THRU -1).
MISSING VALUES qf_ac1_anxieux (LOWEST THRU -1).
MISSING VALUES qa_i19_deplacer (LOWEST THRU -1).
MISSING VALUES qa_i19_escalier (LOWEST THRU -1).
MISSING VALUES qa_i19_exterieur (LOWEST THRU -1).
MISSING VALUES qa_i19_marcher200 (LOWEST THRU -1).
MISSING VALUES qa_i19_coucherlever (LOWEST THRU -1).
MISSING VALUES qa_i19_habiller (LOWEST THRU -1).
MISSING VALUES qa_i19_manger (LOWEST THRU -1).
MISSING VALUES qa_i19_laver (LOWEST THRU -1).
MISSING VALUES qf_ac3_continence (LOWEST THRU -1).
MISSING VALUES qf_ac3_telephoner (LOWEST THRU -1).
MISSING VALUES qf_ac3_courses (LOWEST THRU -1).
MISSING VALUES qf_ac3_repas (LOWEST THRU -1).
MISSING VALUES qf_ac3_menage (LOWEST THRU -1).
MISSING VALUES qf_ac3_lessive (LOWEST THRU -1).
MISSING VALUES qf_ac3_transports (LOWEST THRU -1).
MISSING VALUES qf_ac3_medicaments (LOWEST THRU -1).
MISSING VALUES qf_ac3_finances (LOWEST THRU -1).
MISSING VALUES qf_aq3_menage (LOWEST THRU -1).
MISSING VALUES qf_aq3_repas (LOWEST THRU -1).
MISSING VALUES qf_aq3_courses (LOWEST THRU -1).
MISSING VALUES qf_aq3_reparations (LOWEST THRU -1).
MISSING VALUES qf_aq3_remplir_impot (LOWEST THRU -1).
MISSING VALUES qf_aq3_aider_toilette (LOWEST THRU -1).
MISSING VALUES qf_aq3_presence (LOWEST THRU -1).
MISSING VALUES qf_aq3_emmener_promenade (LOWEST THRU -1).
MISSING VALUES qf_aq3_aide_financiere (LOWEST THRU -1).
MISSING VALUES qf_aq7_menage (LOWEST THRU -1).
MISSING VALUES qf_aq7_repas (LOWEST THRU -1).
MISSING VALUES qf_aq7_courses (LOWEST THRU -1).
MISSING VALUES qf_aq7_reparations (LOWEST THRU -1).
MISSING VALUES qf_aq7_remplir_impot (LOWEST THRU -1).
MISSING VALUES qf_aq7_aider_toilette (LOWEST THRU -1).
MISSING VALUES qf_aq7_presence (LOWEST THRU -1).
MISSING VALUES qf_aq7_emmener_personne (LOWEST THRU -1).
MISSING VALUES qa_j1_infirmiere (LOWEST THRU -1).
MISSING VALUES qa_j1_service_aide (LOWEST THRU -1).
MISSING VALUES qa_j1_physiotherapeute (LOWEST THRU -1).
MISSING VALUES qa_j1_assistant_social (LOWEST THRU -1).
MISSING VALUES qa_j1_repas (LOWEST THRU -1).
MISSING VALUES qa_j1_hopital_jour (LOWEST THRU -1).
MISSING VALUES qa_j1_benevole (LOWEST THRU -1).
MISSING VALUES qa_j1_employe_menage (LOWEST THRU -1).
MISSING VALUES qa_j1_autre (LOWEST THRU -1).
MISSING VALUES qa_j2 ("-7", "-3", "-2").
MISSING VALUES qa_l1_chezvous (LOWEST THRU -1).
MISSING VALUES qa_l1_chezeux (LOWEST THRU -1).
MISSING VALUES qa_l1_telephone (LOWEST THRU -1).
MISSING VALUES qa_l2_chezvous (LOWEST THRU -1).
MISSING VALUES qa_l2_chezeux (LOWEST THRU -1).
MISSING VALUES qa_l2_telephone (LOWEST THRU -1).
MISSING VALUES qa_f1_radio (LOWEST THRU -1).
MISSING VALUES qa_f1_television (LOWEST THRU -1).
MISSING VALUES qa_f1_journal (LOWEST THRU -1).
MISSING VALUES qa_f1_livre (LOWEST THRU -1).
MISSING VALUES qa_f1_internet (LOWEST THRU -1).
MISSING VALUES qf_at1 (LOWEST THRU -1).
MISSING VALUES qf_at3 (LOWEST THRU -1).
MISSING VALUES qf_at5 (LOWEST THRU -1).
MISSING VALUES qf_at7 (LOWEST THRU -1).
MISSING VALUES qf_at11 (LOWEST THRU -1).
MISSING VALUES qf_at13 (LOWEST THRU -1).
MISSING VALUES qf_at15 (LOWEST THRU -1).
MISSING VALUES qf_at17 (LOWEST THRU -1).
MISSING VALUES qf_at19 (LOWEST THRU -1).
MISSING VALUES qf_at21 (LOWEST THRU -1).
MISSING VALUES qf_at23 (LOWEST THRU -1).
MISSING VALUES qf_at25 (LOWEST THRU -1).
MISSING VALUES qf_at27 (LOWEST THRU -1).
MISSING VALUES qf_at29 (LOWEST THRU -1).
MISSING VALUES qf_at31 (LOWEST THRU -1).
MISSING VALUES qf_at33 (LOWEST THRU -1).
MISSING VALUES qf_at35 (LOWEST THRU -1).
MISSING VALUES qf_at37 (LOWEST THRU -1).
MISSING VALUES qf_at39 (LOWEST THRU -1).
MISSING VALUES qf_at41 (LOWEST THRU -1).
MISSING VALUES qf_at43 (LOWEST THRU -1).
MISSING VALUES qf_at8_sport1 ("-7", "-3", "-2").
MISSING VALUES qf_at8_sport2 ("-7", "-3", "-2").
