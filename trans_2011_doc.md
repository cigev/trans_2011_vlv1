# « Vivre – Leben – Vivere », étude sur les conditions de vie et de santé des personnes âgées de plus de 65 ans en Suisse

«Vivre – Leben – Vivere» est une enquête transversale sur la santé des personnes âgées dans les cantons de Genève, du Valais, du Tessin, de Bâle et de Berne, réalisée en 2011-2012 par le CIGEV (Centre interfacultaire de gérontologie et d'études des vulnérabilités). Également intitulée «Old Age Democratization? Progresses ans Inequalities in Switzerland», l'étude a été financée par le Fonds national suisse de la recherche scientifique (FNS), par le Pôle de recherche national «LIVES – Surmonter la vulnérabilité: perspective du parcours de vie» et par Pro Senectute.

Ce projet propose d'investiguer les conditions de vie de la population âgée en Suisse et d'aborder la diversité de ces conditions par une approche interdisciplinaire. L'étude se base sur une enquête conduite en 2011-2012 dans deux cantons francophones, deux cantons germanophones et un canton italophone. Dans le cas des cantons francophones, les données récoltées pourront être comparées aux enquêtes précédentes, conduites en 1979 et 1994, dans le but d'estimer les changements et la continuité dans les conditions de vie des séniors.

Le vieillissement est un défi majeur dans les pays industrialisés, et la Suisse ne fait pas exception. De fait, au cours du dernier siècle, la proportion d'individus âgés de 60 ans et plus a triplé, alors que la proportion de personnes de plus de 80 ans a été multipliée par huit! De plus, dans les dernières décennies, l'attente d'une vie sans problème de santé majeur s'est accrue plus rapidement que l'espérance de vie, tandis que la prévalence de la dépendance a diminué parmi les séniors.

Des changements positifs substantiels se sont produits entre 1979 et 1994. Cependant, rien ne garantit qu'une tendance semblable persiste. De plus, les caractéristiques de la population âgée en 2011 ne peuvent être prédites de manière satisfaisante sur la base des données précédentes. De fait, la structure de la population âgée a changé drastiquement au cours des dernières décennies. Plus important encore, de nouvelles générations portant leurs propres spécificités atteindront prochainement l'âge de la retraite. Le vieillissement massif constaté dans la population immigrée en Suisse constitue un exemple clair de ce changement de composition.

En raison de ces divers changements, ce projet cherche à aborder simultanément deux questions majeurs: l'hétérogénéité parmi les personnes âgées, c'est-à-dire la diversité et les inégalités, et la durabilité des tendances positives observées précédemment en termes de participation sociale, santé et longévité.

L'approche théorique est centrée sur le concept de ressources, et conçue dans la théorie du développement humain lifespan proposée par le psychologue Paul Baltes, enrichie par les sociologues dans le paradigme du parcours de vie. Globalement, cette recherche analyse la manière dont les ressources sont construites à travers les vies des individus, intégrées dans des trajectoires familiales et des contextes socioéconomiques, culturels et politiques. Ainsi, le projet évalue d'abord comment la santé, la famille, la résidence et les trajectoires professionnelles au cours de la vie ont construit l'ensemble de ressources disponibles pour les individus âgés. Il essaie de plus d'évaluer la diversité de ces ressources et la manière avec laquelle elles sont gérées par les individus pour maintenir au mieux une vie active, un haut niveau de bien-être, l'autonomie et la santé.

Après avoir connecté le passé au présent, le projet analyse également comment, dans les expériences actuelles du vieillissement, les ressources individuelles disponibles interagissent avec les ressources socioculturelles accessibles. Dans cette perspective, la comparaison de cinq régions politiques différentes (les cantons de Bâle, de Berne, de Genève, du Tessin et le Valais central) est hautement profitable. Plus encore, dans les régions francophones (canton de Genève et Valais central), le projet bénéficie de la possibilité d'aborder les changements historiques récents à travers une comparaison des résultats avec une nouvelle analyse des enquêtes menées en 1979 et 1994. Cela constitue une opportunité unique sur le continent européen d'examiner l'évolution de la population âgée au cours des 30 dernières années.

Enfin, une approche interdisciplinaire basée sur les ressources apporte un outil puissant avec lequel on peut identifier les prédicteurs les plus pertinents du bien-être, dans le passé et dans le présent, de même que les leviers sur lesquels l'action individuelle et les politiques sociales peuvent appuyer pour prédire les pertes ou promouvoir les processus de vieillissement réussi.

Pour faire face au défi de fournir une image mise à jour et plus représentative du vieillissement en Suisse, ce projet regroupe des dizaines de chercheuses et chercheurs de 7 disciplines: la gériatrie, la psychogériatrie, la psychologie, la sociologie, l'étude des politiques sociales, la démographie et la socioéconomie. Enracinés dans une modèle théorique commun, des concepts partagés et des objectifs communs, les chercheuses et chercheurs veulent ensemble résoudre la tension entre continuité (comparaison de nos résultats avec ceux des études menées en 1979 et 1994) et innovation (meilleurs outils, nouvelles questions, et représentation plus nationale).

## Citation

(FR)
> Oris, M. (dir.). (2011). *Vivre – Leben – Vivere* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Oris, M. (Dir.). (2011). *Vivre – Leben – Vivere* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

## Auteur·e·s

* **Michel Oris** – *Requérant principal*
* **Stefano Cavalli** – *Chef de projet*
* **Catherine Ludwig** – *Cheffe de projet*
* **Jean-François Bickel** – *Co-requérant*
* **Claudio Bolzman** – *Co-requérant*
* **Alessandra Canuto** – *Co-requérante*
* **Dominique Joye** – *Co-requérant*
* **Christophe Luthy** – *Co-requérant*
* **Pasqualina Perrig-Chiello** – *Co-requérante*
* **Dario Spini** – *Co-requérant*
* **Éric Widmer** – *Co-requérant*

### Collaboratrices et collaborateurs

* **Marie Baeriswyl**
* **Christine Cedraschi**
* **Étienne Christe**
* **Nora Dasoki**
* **Daniela Dus**
* **Aline Duvoisin**
* **Delphine Fagot**
* **Rainer Gabriel**
* **Myriam Girardin**
* **Julia Henke**
* **François Hermann**
* **Sara Hutchison**
* **Laure Kaeser**
* **Matthias Kliegel**
* **Katja Margelisch**
* **Barbara Masotti**
* **Christophe Monnot**
* **Marthe Nicolet**
* **Alessandra Rosciano**
* **Stefanie Spahni**
* **Aude Tholomier**
* **Angelica Torres Florez**
* **France Weaver**
* **Kerstin Weber**

### Administration et technique

* **Nathalie Blanc**
* **Claire Grela**
* **Grégoire Métral**

## Financement

* Fonds national suisse de la recherche scientifique (FNS) ([Sinergia](http://p3.snf.ch/Project-129922))
* Pôle de recherche national «LIVES – Surmonter la vulnérabilité: perspective du parcours de vie» ([PRN LIVES](https://www.lives-nccr.ch/fr/page/grand-age-n66))
* [Pro Senectute](https://prosenectute.ch/)

## Thèmes

Interviewé: sexe, âge à l'entretien, date de naissance, nationalité, langues, état civil, éducation, parents, enfance heureuse, nombre d'enfants, de petits-enfants, d'arrière-petits-enfants, confession, pratique religieuse.

Activité et logement: activité actuelle, retraite, logement, équipements, nombre de personnes dans le logement, animal de compagnie, médias et langues, déménagements. Premier métier et taux, dernier métier et taux.

Participation sociale et politique: votations et élections, associations et clubs (actuel et à 45 ans), responsabilités.

Mobilité: moyens de transport.

Santé: poids, taille, état de santé, douleurs selon région, vue, ouïe, chutes, activités de la vie quotidienne, aides à domicile, tabac, alcool, visites, correspondance, téléphone, problèmes dans les 4 dernières semaines; maladie actuelle, accident, opération, difficultés de déplacement, hospitalisations, problèmes généraux de santé (infarctus, hypertension, diabète, ulcère, douleurs, etc.), localisation et intensité. Douleurs au dos.

Changements: nature du changement, tournant.

Opinions et préoccupations: aides financières, charge pour les difficultés de santé, aides ménagères, héritage, jeunes et vieux, aide sociale, maladie, conflits, amis, agression, vol, soi-même; relations avec administrations. Opinions sur la société: demandes aux enfants, rester chez soi, respect des personnes âgées, laisser-faire, vie trop facile, femme au foyer.

Enfant(s) de l'interviewé (pour chaque enfant): sexe, année de naissance, année du décès, nationalités, état civil, nombre d'enfant(s), lieu de résidence, activité principale, activité principale du conjoint.

Famille de l'interviewé: nombre de frères et sœurs, nombre de frères/de sœurs encore vivants aujourd'hui.

Conjoint: sexe, année de naissance, nationalité, année de mise en couple, mariage, enfants, relation, situation professionnelle partenaire, dernier métier. Précédent partenaire: mêmes questions, raison de la séparation, problèmes suite à la séparation, aides. Grand amour: mêmes questions.

Migration: motif de la venue en Suisse, partenaire avant, enfants, regroupement familial, retour dans le pays d'origine, lieu d'inhumation.

Cognition: vocabulaire, suite de chiffres, suite de lettres, phrase, dessin, date complète, lieu, mémoire, décompte depuis 100, mot à l'envers, énoncés.

État général: appétit, amis, sommeil, plaisir, tristesse, confiance, déprime, fatigue, soucis, irritation, confiance, angoisse. Difficultés quotidiennes. Médicaments.

Réseau familial et social: personnes significatives, sexe, âge, éducation, lien, lieu de résidence, contact, confiance. Soutien émotionnel, soutien matériel, changer d'avis, conflits. Jeunes de la famille (16-25 ans): activités sociales, activités de loisir, discussions, repas, conseils. Jeunes à l'extérieur de la famille: activités sociales, activités de loisir, discussions, repas, conseils. Services rendus et reçus: ménage, repas, courses, bricolage, vêtements, spectacles, travail. garde d'enfants, toilette, compagnie, administration, aide financière. Amis et connaissances: attachement, services rendus et reçus. Activités bénévoles.

Ressources financières: salaire, rente AVS, caisse de retraite, autres revenus, location, chômage, AI, autres. Revenu mensuel du ménage, fortune.

Activités (aujourd'hui et à 45 ans): promenades à pied, jardinage, gymnastique, sport, café/restaurant, cinéma/concert/théâtre, excursions 1-2 jours, voyages 3-4 jours au moins, musique, activités artistiques, suivre des cours, jeux de société, mots croisés, travaux manuels, manifestations politiques/syndicales, manifestations villageoises/de quartier, manifestations sportives, prier, pratique religieuse, maison de quartier.

Associations pour personnes âgées: connaissance des associations, notamment Pro Senectute.

## Univers de référence

Population suisse de 65 ans et plus.

## Échantillonnage

Enquête transversale réalisée auprès d'un échantillon représentatif de la population âgée de 65 ans et plus, stratifié par âge, sexe et région (cantons de Bâle, Berne, Genève, Tessin et Valais central). L'échantillon de base était de 3600 individus.

Les non-réponses et les refus ont été remplacés par la méthode «des jumeaux» (remplacement d'une personne de l'échantillon original qui se révèle impossible à joindre par une autre, présentant les mêmes caractéristiques connues sur la base des données des registres de population).

Participation: échantillon de 3600 individus. Un sur-échantillon de personnes issues de l'immigration a été réalisé, avec 500 personnes interviewées. Dans le cadre du projet «linked lives», 100 partenaires devaient également être interviewés, mais les données ne sont pas disponibles.

## Récolte des données

Instruments: questionnaire auto-administré (QAA), calendrier de vie (CV), questionnaire face-à-face (QFF). Les deux premiers ont été remplis sur papier par la personne interviewée, puis numérisés par des auxiliaires de recherche, le troisième a été rempli par l'enquêteur/trice avec un ordinateur portable. Pour les personnes qui n'étaient pas en mesure de répondre au questionnaire, une version raccourcie du questionnaire (QP) a été proposée à un «proxy» (conjoint·e, enfant, proche, aide soignant·e, etc.). Les questionnaires sont indiqués par les lettres «qp» et les données se trouvent dans un jeu de données séparé (trans_2011_data_proxy.csv). Les données des calendriers de vie ne sont pas incluses ici, car incomplètement codées et anonymisées.

Les enquêteurs/trices ont été mandatés et formés par le CIGEV.

Date du relevé: entre janvier 2011 et décembre 2012.

## Métadonnées

* Relevé effectué par: Université de Genève, CIGEV.
* Documentation: questionnaires QAA, AFF et QP, calendriers de vie, feuillets d'entretien, planches, brochure, codebook.
* Cas: 3080 pour les QAA+QFF, 555 pour le QP.
* Variables: 1208 conservées pour le QAA+QFF, 178 conservées pour le QP.
* Format: CSV + syntaxe SPS pour importation dans SPSS.

## Licence

[CC-BY-SA (Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International)](LICENSE)

## Rapport de traitement

### Réalisation

* [CIGEV](https://cigev.unige.ch/) (Grégoire Métral) – 2017-2019, sur la base d'un premier nettoyage par les collaboratrices et collaborateurs mentionnés ci-dessus.

### Mini-contrôle

#### Description du projet et de la méthode
La description du projet de recherche se trouve dans les premiers paragraphes de cette documentation. La méthode est détaillée dans deux publications de 2014 et 2016:
* Ludwig, C., Cavalli, S., & Oris, M. (2014). "Vivre/Leben/Vivere": An interdisciplinary survey addressing progress and inequalities of aging over the past 30 years in Switzerland. *Archives of Gerontology and Geriatrics*, 59, 240–248. [doi:10.1016/j.archger.2014.04.004](https://www.doi.org/10.1016/j.archger.2014.04.004)
* Oris, M., Guichard, E., Nicolet, M., Gabriel, R., Tholomier, A., Monnot, C., … Joye, D. (2016). Representation of vulnerability and the elderly. A Total Survey Error perspective on the VLV survey. In M. Oris, C. Roberts, D. Joye, & M. Ernst Staehli (Eds.), *Surveying human vulnerabilities across the life course* (pp. 27–64). New York: Springer. [10.1007/978-3-319-24157-9_2](https://www.doi.org/10.1007/978-3-319-24157-9_2)

#### Questionnaires
Le questionnaire est subdivisé en trois fascicules: questionnaire auto-administré (QAA), calendrier de vie (CV) et questionnaire face-à-face (QFF) qui se trouvent dans les fichiers PDF ci-joint. Un feuillet d'entretien a été utilisé pendant l'entretien face-à-face, comprenant notamment des exercices à réaliser (PDF également à disposition).

Pour les personnes qui ne pouvaient pas répondre elles-mêmes au questionnaire, un questionnaire proxy (QP) a été proposé à un·e proche, avec un nombre limité de questions.

#### Instructions aux enquêteurs/trices
Des formations d'une semaine ont été données aux enquêteurs/trices pour chaque canton. Le feuillet d'entretien comprend en outre l'essentiel des instructions aux enquêteurs/trices. Les questionnaires comportent également des instructions dans les en-têtes des questions.

#### Matériel annexe
Le feuillet d'entretien et les planches sont présentes.

#### Évaluation de la documentation
La documentation est suffisante.

#### Évaluation du ou des fichier(s) de données
Le jeu de données principal fourni pas les chercheurs est une construction de fichiers à partir des saisies (numérisation des QAA) et des exportations de données des ordinateurs portables (QFF). Un travail de codification a été réalisé pour la plupart des questions textuelles.

Les données du questionnaire proxy sont rassemblées dans un jeu de données séparé.

Les différents fichiers des CV sont une saisie brute des informations contenues. La codification a été partiellement réalisée dans le cadre de mémoires de master ou de thèses. Les données codifiées des CV n'ont à ce jour pas été récupérées et nettoyées.

### Variables

La matrice des données qui englobe les variables des 2 questionnaires principaux (QAA et QFF) a été réalisée. Les noms des variables correspondent au questionnaire concerné (qa_xxx et qf_xxx), suivi du code de la question. Pour le QP, les noms des variables ont été repris des QAA et QFF puisqu'il s'agit d'un set limité de questions issues des QAA et QFF. L'ordre conservé est cependant celui du QP.

#### Anonymisation
Pour des raisons liées à l'anonymisation des données, certaines variables ont été supprimées ou corrigées dans les jeux de données fournis ici. Il s'agit des variables suivantes:
* date_entretien_1 (date du premier rendez-vous): tous les entretiens ont été réalisés entre 2011 et 2012 (quelques rares entretiens de pré-test en décembre 2010); la variable a été éliminée
* age_entretien (âge à l'entretien, au centième): la variable a été éliminée
* qa_a1 (quelle est votre date de naissance?): seule l'année de naissance a été conservée
* qf_aq2_personne1 (personnes de la famille, à coder): la variable a été éliminée
* qf_aq2_personne2 (personnes de la famille, à coder): la variable a été éliminée
* qf_aq4_personne1 (personnes de la famille, à coder): la variable a été éliminée
* qf_aq4_personne2 (personnes de la famille, à coder): la variable a été éliminée

#### Présence des labels de variables
Avec la syntaxe fournie, tous les labels de variables sont indiqués sous forme du libellé de la question, repris du questionnaire. Les labels sont parfois tronqués en raison des limitations de SPSS, mais la question complète peut être retrouvée dans les fichiers PDF des questionnaires.

#### Variables construites et corrections
Quelques variables construites sont présentes au début du jeu de données: groupement par canton (ou région pour BE), groupe d'âge, âge à l'entretien... D'autres variables construites se trouvent en fin de matrice (indice de Wang): elles ont été conservées, même si la construction de cet indice est manquante (il faudrait probablement lire les publications relatives aux enquêtes de 1979 et 1994). Les variables construites correspondantes en 1979 et 1994 n'ont pas été conservées.

Le nettoyage de base a consisté en les opérations suivantes:
* modification de tous les labels de variables en reprenant le libellé de la question
* modification de tous les labels de valeurs en reprenant les libellés des items
* vérification de toutes les mesures (nominal, ordinal, scale)
* définition de tous les missings en négatif en gardant la [convention expliquée ci-dessous](#missing)
* codification de certaines variables string (qa_b12 et qa_b13 notamment)
* suppression des variables qf_n1 à qf_s10 qui sont vides (nombre maximal d'enfants dans l'échantillon = 9)
* correction de certains noms de variables avec fautes d'orthographe (p. ex. portuguais -> portugais)

Les corrections de base ont porté sur les points suivants:
* recodage des lieux de résidence, ajout des codes oubliés, vérification et correction des codes sauvages, élimination des variables ayant permis la codification
* adoption d'une logique systématique pour les sexes (1=Femme, 2=Homme)
* transformation en -7 (INAP) tous les missings qui correspondaient à des questions n'ayant manifestement pas été posées (filtres)
* correction de nombreuses erreurs et incohérences (parfois en reprenant les questionnaires papier, parfois en utilisant le code -3 (NV)
* correction de toutes les incohérences sur le nombre d'enfants, leurs dates de naissance, les petits-enfants: 3 chercheuses de l'équipe VLV ont travaillé sur ce point et leurs résultats ont été intégrés

Toutes les autres corrections ont été consignées dans un document annexe. La syntaxe de correction pour les enfants et petits-enfants, permettant de voir les corrections effectuées, est aussi disponible dans un document séparé. Ces deux fichiers sont disponibles sur demande, mais ne sont pas fournis ici.

Certaines incohérences demeurent:
* les questions filtres de la section D du QAA n'ont pas été respectées (pas corrigé)
* la question filtre E5 (pour la question E7) du QAA n'a pas été respectée (pas corrigé)
* pour la question qf_ar1*: l'individu 111005 répond "oui" à toutes les questions...

#### Variables contextuelles
Quelques variables contextuelles figurent au début du questionnaire: code de l'encodeur (= personne qui a fait la codification), code de l'enquêtrice ou de l'enquêteur, consentement pour l'utilisation des données à des fins pédagogiques obtenu, date du premier rendez-vous.

Le genre d'EGO a été obtenu par l'Office fédéral de la statistique (OFS) dans l'échantillon. La question a été posée dans le questionnaire face-à-face.

#### Variable(s) de pondération
Le nombre de personnes interrogées par classe d'âge est à peu près le même, ce qui entraîne un biais (il y a beaucoup moins de personnes de 95 ans et plus dans la population que de personnes de 65 à 69 ans). Un coefficient de pondération a été calculé en prenant en compte l'âge et le sexe pour chaque canton (une variable canton2, regroupant les 3 zones du canton de Berne, a été créée à cet effet). La formule est la suivante:

	coeff = [cat(pop) x tot(ech)] / [tot(pop) x cat(ech)]

Par exemple:

	coeff (F 65-69 GE) = [(F 65-69 à GE) x (Nb de répondants total dans base)] / [(Nb de Genevois de 65-69) X (Nb de répondants F 65-69 dans base)]
	= (12083 * 704) / (74643 * 59) = 1.93155

*Tableau 1: Taille des échantillons et des populations*

[Voir annexe](trans_2011_doc_table1.md)

*Tableau 2: Coefficients de pondération par classe d'âge et sexe*

[Voir annexe](trans_2011_doc_table2.md)

La variable de pondération (pond_vlv) a été placée tout à la fin du jeu de données.

#### Variables disparues
Aucune.

### Valeurs

Tous les labels de valeurs ont été insérés selon le questionnaire.

Les filtres («si oui, passer à la question xxx») ont été pris en compte et les INAP ont été recodés.

Les codes sauvages constatés ont été mis en missing (voir ci-dessous).

Pour les mesures (scale, ordinal, nominal): le type a été indiqué systématiquement.

Quelques tests de plausibilité ont été effectués, notamment sur les années, mais pas systématiquement.

Toutes les questions ont été passées en revue et des corrections ont été faites lorsque des incohérences étaient constatées (voir [Variables construites et corrections](#variables-construites-et-corrections) ci-dessus).

#### Missing
Les non réponses ou données manquantes n'étaient pas documentées. Nous les avons systématiquement indiquées avec des valeurs négatives selon la logique suivante:

* -1 = Ne sait pas (NSP)
* -2 = Non réponse (NR)
* -7 = Inapproprié (INAP), en raison d'un filtre
* -9 = Valeur manquante non documentée (Missing)

D'autres valeurs négatives sont présentes lorsque la valeur indiquée était manifestement erronnée, par exemple -3: Non valide (NV).

La syntaxe fournie permet de fixer en "missing" toutes les valeurs négatives.

## Codebook

Deux codebooks sont fournis pour les données QAA+QFF et QP.
* [trans_2011_codebook.ods](trans_2011_codebook.ods) – Codebook QAA + QFF au format OpenDocument (feuille de calcul)
* [trans_2011_codebook_proxy.ods](trans_2011_codebook_proxy.ods) – Codebook QP au format OpenDocument (feuille de calcul)

## Publications

* Baeriswyl, M. (2017). Participations sociales au temps de la retraite. In N. Burnay & C. Hummel (Eds.) *Vieillissement et classes sociales*, Population, Family, and Society; 27 (pp. 141–170). Bern: Peter Lang.
* Bolzman, C. (2012). Democratization of ageing: also a reality for elderly immigrants? *European Journal of Social Work*, 15(1), 97–113. [doi:10.1080/13691457.2011.562018](https://www.doi.org/10.1080/13691457.2011.562018)
* Bolzman, C. (2015a). Older migrants and care. What place for the family? In Aa.Vv. (Ed.), *La famiglia tra fragilità e risorse* (Vol. 259, pp. 355–373). Brescia, Italy: La Scuola.
* Bolzman, C. (2015b). Personnes âgées, migrations et care. Enjeux intergénérationnels et pour les politiques sociales. In N. Hajji & O. Lescarret (Eds.), *Les mouvements sociaux à l’épreuve de l’interculturel* (pp. 189–209). Paris: L’Harmattan.
* Bolzman, C., & Kaeser, L. (2012). Active Ageing and Immigrants Elders: A Possible Relation? Exploring the Case of Switzerland. *Revista Migrações*, 10, 29–44.
* Bolzman, C., & Kaeser, L. (2016). Active ageing and older immigrants: The role of health and economic factors related to retirement. *Salute e Società*, XV(1), 165–179. [doi:10.3280/SES2016-001012](https://www.doi.org/10.3280/SES2016-001012)
* Bolzman, C., Kaeser, L., & Christe, E. (2017). Transnational Mobilities as a Way of Life Among Older Migrants from Southern Europe. *Population, Space and Place*, 23(5), 1–13. [doi:10.1002/psp.2016](https://www.doi.org/10.1002/psp.2016)
* Bolzman, C., & Vagni, G. (2015). Égalité de chances? Une comparaison des conditions de vie des personnes âgées immigrées et “nationales.” *Hommes et Migrations*, 1309, 19–28.
* Bolzman, C., & Vagni, G. (2017). Forms of care among native Swiss and older migrants from Southern Europe: a comparison. *Journal of Ethnic and Migration Studies*, 43(2), 250–269. [doi:10.1080/1369183X.2016.1238908](https://www.doi.org/10.1080/1369183X.2016.1238908)
* Bolzman, C., & Vagni, G. (2018). ‘And we are still here’: Life Courses and Life Conditions of Italian, Spanish and Portuguese Retirees in Switzerland. In I. Vlase & B. Voicu (Eds.), *Gender, Family, and Adaptation of Migrants in Europe: A Life Course Perspective* (pp. 67–89). Cham: Springer International Publishing. [doi:10.1007/978-3-319-76657-7_4](https://www.doi.org/10.1007/978-3-319-76657-7_4)
* Cavalli, S., Dasoki, N., Dus, D., Masotti, B., & Rosciano, A. (2015). Condizioni di salute, benessere e scambi di servizi nella popolazione anziana residente in Ticino. In F. Giudici, S. Cavalli, M. Egloff, & B. Masotti (Eds.), *Fragilità e risorse della popolazione anziana in Ticino* (pp. 47–66). Bellinzona, Switzerland: Ufficio di statistica.
* Cavalli, S., & Dus, D. (2015). Terza età e vulnerabilità. In F. Giudici, S. Cavalli, M. Egloff, & B. Masotti (Eds.), *Fragilità e risorse della popolazione anziana in Ticino* (pp. 67–86). Bellinzona, Switzerland: Ufficio di statistica.
* Cedraschi, C., Luthy, C., Allaz, A.-F., Herrmann, F. R., & Ludwig, C. (2016). Low back pain and health-related quality of life in community-dwelling older adults. *European Spine Journal*, 25(9), 2822–2832. [doi:10.1007/s00586-016-4483-7](https://www.doi.org/10.1007/s00586-016-4483-7)
* Dasoki, N., Morselli, D., & Spini, D. (2016). La mémoire autobiographique à travers le parcours de vie: évaluation rétrospective du bonheur et de la vulnérabilité. *Canadian Journal on Aging / La Revue Canadienne Du Vieillissement*, 35(3), 308–318. [doi:10.1017/S0714980816000362](https://www.doi.org/10.1017/S0714980816000362)
* Desrichard, O., Vallet, F., Agrigoroaei, S., Fagot, D., & Spini, D. (9998). Frailty in aging and its influence on perceived stress exposure and stress-related symptoms: evidence from the Swiss Vivre/Leben/Vivere study. *European Journal of Ageing*. [doi:10.1007/s10433-017-0451-2](https://www.doi.org/10.1007/s10433-017-0451-2)
* Duvoisin, A. (2016). Trajectoires familiales et professionnelles durant le baby boom en Suisse: continuités et ruptures parmi les générations féminines. In S. Pennec, C. Girard, & J.-P. Sanderson (Eds.) *Trajectoires et âges de la vie* (pp. 1–19). Paris: AIDELF. Retrieved from https://www.erudit.org/fr/livres/actes-des-colloques-de-lassociation-internationale-des-demographes-de-langue-francaise/trajectoires-ages-vie-selection-darticles-issus-travaux-presentes-au-xviiie--978-2-9521220-5-4/
* Duvoisin, A., Baeriswyl, M., Oris, M., Perrig-Chiello, P., & Bickel, J.-F. (2012). *Pro Senectute: Visibilité et usages d’une association bientôt centenaire. Au service des vulnérables? / Pro Senectute: Sichtbarkeit und Nutzung einer bald hundertjährigen Stiftung. Im Dienste vulnerabler Menschen?* (Pro Senectute report). Genève: CIGEV.
* Duvoisin, A., Burgnard, S., & Oris, M. (2016). Childless People during the Baby Boom in Switzerland. *Annales de Démographie Historique*, (132), 193–221. [doi:10.3917/adh.132.0193](https://www.doi.org/10.3917/adh.132.0193)
* Egloff, M., Cavalli, S., & Giudici, F. (2015). Anzianità: Una realtà difficilmente oggettivabile. In F. Giudici, S. Cavalli, M. Egloff, & B. Masotti (Eds.), *Fragilità e risorse della popolazione anziana in Ticino* (pp. 11–18). Bellinzona, Switzerland: Ufficio di statistica.
* Gabriel, R., Oris, M., Studer, M., & Baeriswyl, M. (2015). The persistence of social stratification? A life course perspective on poverty in old-age in Switzerland. *Swiss Journal of Sociology*, 41(3), 465–487.
* Girardin, M., & Widmer, É. (2015). Lay definitions of family and social capital in later life. *Personal Relationships*, 22(4), 712–737. [doi:10.1111/pere.12107](https://www.doi.org/10.1111/pere.12107)
* Giudici, F., Cavalli, S., Egloff, M., & Masotti, B. (Eds.). (2015). *Fragilità e risorse delle persone anziane residenti in Ticino*. Bellinzona, Switzerland: Ufficio di statistica. Retrieved from http://www3.ti.ch/DFE/DR/USTAT/index.php?fuseaction=pubblicazioni.dettaglioVolume&idCollana=121&idVolume=1481
* Giudici, F., Egloff, M., Cavalli, S., & Masotti, B. (2015). Anziani al di là dei cliché: La sfida di una realtà molteplice. In F. Giudici, S. Cavalli, M. Egloff, & B. Masotti (Eds.), *Fragilità e risorse della popolazione anziana in Ticino* (pp. 129–132). Bellinzona, Switzerland: Ufficio di statistica.
* Henke, J. (2016). Définir la vulnérabilité auprès des personnes âgées en Suisse : l’importance de la mesure subjective. In M. Oris, P. Cordazzo, G. Bellis, E. Brown, & A. Parant (Eds.) *Les populations vulnérables. Actes du XVIe colloque national de démographie*. Bordeaux: CUDEP (Conférence Universitaire de Démographie et d’Étude des Populations). Retrieved from http://cudep.u-bordeaux4.fr/sites/cudep/IMG/pdf/Ch1-08_HENKE.pdf
* Höpflinger, F., Spahni, S., & Perrig-Chiello, P. (2013). Persönliche Bilanzierung der Herausforderungen einer Verwitwung im Zeit- und Geschlechtervergleich. *Zeitschrift für Familienforschung*, 25(3), 267–285.
* Ihle, A., Gouveia, É. R., Gouveia, B. R., Freitas, D. L., Jurema, J., Odim, A. P., & Kliegel, M. (2017). The relation of education, occupation, and cognitive activity to cognitive status in old age: the role of physical frailty. *International Psychogeriatrics*, 29(9), 1469–1474. [doi:10.1017/S1041610217000795](https://www.doi.org/10.1017/S1041610217000795)
* Ihle, A., Gouveia, É. R., Gouveia, B. R., Linden, B. W. A. van der, Sauter, J., Gabriel, R., … Kliegel, M. (2017). The Role of Leisure Activities in Mediating the Relationship between Physical Health and Well-Being: Differential Patterns in Old and Very Old Age. *Gerontology*, 63(6), 560–571. [doi:10.1159/000477628](https://www.doi.org/10.1159/000477628)
* Ihle, A., Grotz, C., Adam, S., Oris, M., Fagot, D., Gabriel, R., & Kliegel, M. (2016). The association of timing of retirement with cognitive performance in old age: the role of leisure activities after retirement. *International Psychogeriatrics*, 28(10), 1659–1669. [doi:10.1017/S1041610216000958](https://www.doi.org/10.1017/S1041610216000958)
* Ihle, A., Jopp, D., Oris, M., Fagot, D., Kliegel, M., Ihle, A., … Kliegel, M. (2016). Investigating Discontinuity of Age Relations in Cognitive Functioning, General Health Status, Activity Participation, and Life Satisfaction between Young-Old and Old-Old Age. *International Journal of Environmental Research and Public Health*, 13(11), 1092. [doi:10.3390/ijerph13111092](https://www.doi.org/10.3390/ijerph13111092)
* Ihle, A., Mons, U., Perna, L., Oris, M., Fagot, D., Gabriel, R., & Kliegel, M. (2016). The relation of obesity to performance in verbal abilities, processing speed, and cognitive flexibility in old age: The role of cognitive reserve. *Dementia and Geriatric Cognitive Disorders*, 42(1–2), 117–126. [doi:10.1159/000448916](https://www.doi.org/10.1159/000448916)
* Ihle, A., Oris, M., Baeriswyl, M., & Kliegel, M. (2018). The relation of close friends to cognitive performance in old age: the mediating role of leisure activities. *International Psychogeriatrics*, 1–6. [doi:10.1017/S1041610218000789](https://www.doi.org/10.1017/S1041610218000789)
* Ihle, A., Oris, M., Fagot, D., Baeriswyl, M., Guichard, E., & Kliegel, M. (2015). The association of leisure activities in middle adulthood with cognitive performance in old age: The moderating role of educational level. *Gerontology*, 61(1), 543–550. [doi:10.1159/000381311](https://www.doi.org/10.1159/000381311)
* Ihle, A., Oris, M., Fagot, D., Chicherio, C., Linden, B. W. A. van der, Sauter, J., & Kliegel, M. (2018). Associations of educational attainment and cognitive level of job with old age verbal ability and processing speed: The mediating role of chronic diseases. *Applied Neuropsychology: Adult*, 25(4), 356–362. [doi:10.1080/23279095.2017.1306525](https://www.doi.org/10.1080/23279095.2017.1306525)
* Ihle, A., Oris, M., Fagot, D., & Kliegel, M. (2016). The relation of the number of languages spoken to performance in different cognitive abilities in old age. *Journal of Clinical and Experimental Neuropsychology*, 38(10), 1103–1114. [doi:10.1080/13803395.2016.1197184](https://www.doi.org/10.1080/13803395.2016.1197184)
* Ihle, A., Oris, M., Fagot, D., & Kliegel, M. (2017). No cross-sectional evidence for an increased relation of cognitive and sensory abilities in old age. *Aging & Mental Health*, 21(4), 409–415. [doi:10.1080/13607863.2015.1109055](https://www.doi.org/10.1080/13607863.2015.1109055)
* Ihle, A., Oris, M., Fagot, D., Maggiori, C., & Kliegel, M. (2016). The association of educational attainment, cognitive level of job, and leisure activities during the course of adulthood with cognitive performance in old age: The role of openness to experience. *International Psychogeriatrics*, 28(5), 733–740. [doi:10.1017/S1041610215001933](https://www.doi.org/10.1017/S1041610215001933)
* Ihle, A., Oris, M., Sauter, J., Rimmele, U., & Kliegel, M. (2018). Cognitive Reserve and Social Capital Accrued in Early and Midlife Moderate the Relation of Psychological Stress to Cognitive Performance in Old Age. *Dementia and Geriatric Cognitive Disorders*, 45(3–4), 190–197. [doi:10.1159/000488052](https://www.doi.org/10.1159/000488052)
* Kaeser, L. (2013). Les migrants âgés circonspects face aux chercheurs. *REISO - Revue d’information sociale*. Retrieved from http://www.reiso.org/spip.php?article2961
* Kaeser, L. (2014). «Votre questionnaire est trop policier!» De la distance sociale dans une enquête quantitative auprès d’immigrés retraités résidant en Suisse. *Genèses*, 96(3), 157–174.
* Kaeser, L. (2016a). Adapting quantitative survey procedures: The price for assessing vulnerability? Lessons from a large-scale survey on aging and migration in Switzerland. In M. Oris, C. Roberts, D. Joye, & M. Ernst Staehli (Eds.), *Surveying human vulnerabilities across the life course* (pp. 65–85). New York: Springer. [doi:10.1007/978-3-319-24157-9_3](https://www.doi.org/10.1007/978-3-319-24157-9_3)
* Kaeser, L. (2016b). Enjeux de la collecte de données auprès de populations vulnérables : discussions méthodologiques à partir du cas des populations âgées d’origine immigrée. In M. Oris, P. Cordazzo, G. Bellis, E. Brown, & A. Parant (Eds.) *Les populations vulnérables. Actes du XVIe colloque national de démographie*. Bordeaux: CUDEP (Conférence Universitaire de Démographie et d’Étude des Populations). Retrieved from http://cudep.u-bordeaux4.fr/sites/cudep/IMG/pdf/Ch2-01_KAESER.pdf
* Kaeser, L., & Roch, P.-A. (2015). Le vieillissement actif: élaboration, légitimation et tentatives de diffusion d’un référentiel transversal en Europe. *Critique internationale. Revue comparative de sciences sociales*, 2015(3), 145–169. [doi:10.3917/crii.068.0145](https://www.doi.org/10.3917/crii.068.0145)
* Kaeser, L., & Zufferey, J. (2015). Confronting active ageing with empirical evidence: A structural equation model approach. The case of older migrants living in Switzerland. *Swiss Journal of Sociology*, 41(3), 437–463.
* Lalive d’Épinay, C., & Cavalli, S. (2013). *Le quatrième âge ou la dernière étape de la vie*. Lausanne: Presses polytechniques et universitaires romandes.
* Ludwig, C., Cavalli, S., & Oris, M. (2014). "Vivre/Leben/Vivere": An interdisciplinary survey addressing progress and inequalities of aging over the past 30 years in Switzerland. *Archives of Gerontology and Geriatrics*, 59, 240–248. [doi:10.1016/j.archger.2014.04.004](https://www.doi.org/10.1016/j.archger.2014.04.004)
* Luthy, C., Cedraschi, C., Allaz, A.-F., Herrmann, F. R., & Ludwig, C. (2015). Health status and quality of life: results from a national survey in a community-dwelling sample of elderly people. *Quality of Life Research*, 24(7), 1687–1696. [doi:10.1007/s11136-014-0894-2](https://www.doi.org/10.1007/s11136-014-0894-2)
* Madero-Cabib, I., & Kaeser, L. (2016). How voluntary is the active ageing life? A life-course study on the determinants of extending careers. *European Journal of Ageing*, 13(1), 25–37. [doi:10.1007/s10433-015-0355-y](https://www.doi.org/10.1007/s10433-015-0355-y)
* Masotti, B., & Oris, M. (2015). Il ricorso ai servizi domiciliari e il ruolo della famiglia nella quarta età. In F. Giudici, S. Cavalli, M. Egloff, & B. Masotti (Eds.), *Fragilità e risorse della popolazione anziana in Ticino* (pp. 87–110). Bellinzona, Switzerland: Ufficio di statistica.
* Morselli, D., Dasoki, N., Gabriel, R., Gauthier, J.-A., Henke, J., & Le Goff, J.-M. (2016). Using life history calendars to survey vulnerability. In M. Oris, C. Roberts, D. Joye, & M. Ernst Staehli (Eds.), *Surveying human vulnerabilities across the life course* (pp. 177–199). New York: Springer.
* Nicolet, M., & Oris, M. (2016). Mesures et capture de la vulnérabilité dans une enquête sur les conditions de vie et de santé des personnes âgées. L’expérience de VLV (Vivre-Leben-Vivere) en Suisse. In M. Oris, P. Cordazzo, G. Bellis, E. Brown, & A. Parant (Eds.), *Les populations vulnérables. Actes du XVIe colloque national de démographie*. Bordeaux: CUDEP. Retrieved from http://cudep.u-bordeaux4.fr/seance-2-les-populations.html
* Oris, M. (2016). Ageing and the reconciliation of history and biographies: An approach to fill the gap. In K. Matthijs, S. Hin, J. Kok, & H. Matsuo (Eds.) *The future of historical demography : upside down and inside out* (pp. 126-129). Leuven / Den Haag: ACCO Publishers.
* Oris, M., Gabriel, R., Ritschard, G., & Kliegel, M. (2017). Long Lives and Old Age Poverty: Social Stratification and Life-Course Institutionalization in Switzerland. *Research in Human Development*, 14(1), 68–87. [doi:10.1080/15427609.2016.1268890](https://www.doi.org/10.1080/15427609.2016.1268890)
* Oris, M., Gabriel, R., Studer, M., & Baeriswyl, M. (2016). La construction des inégalités économiques et inégalités de genre dans la vieillesse à travers le parcours de vie (Suisse, 1979-2011). In S. Pennec, C. Girard, & J.-P. Sanderson (Eds.) *Trajectoires et âges de la vie* (pp. 1–14). Paris: AIDELF. Retrieved from https://www.erudit.org/fr/livres/actes-des-colloques-de-lassociation-internationale-des-demographes-de-langue-francaise/trajectoires-ages-vie-selection-darticles-issus-travaux-presentes-au-xviiie--978-2-9521220-5-4/
* Oris, M., Guichard, E., Nicolet, M., Gabriel, R., Tholomier, A., Monnot, C., … Joye, D. (2016). Representation of vulnerability and the elderly. A Total Survey Error perspective on the VLV survey. In M. Oris, C. Roberts, D. Joye, & M. Ernst Staehli (Eds.), *Surveying human vulnerabilities across the life course* (pp. 27–64). New York: Springer. [10.1007/978-3-319-24157-9_2](https://www.doi.org/10.1007/978-3-319-24157-9_2)
* Perrig-Chiello, P., Spahni, S., Höpflinger, F., & Carr, D. (2016). Cohort and gender differences in psychosocial adjustment to later-life widowhood. *The Journals of Gerontology Series B: Psychological Sciences and Social Sciences*, 71(4), 765–774. [doi:10.1093/geronb/gbv004](https://www.doi.org/10.1093/geronb/gbv004)
* Rosciano, A., Dasoki, N., Cavalli, S., & Dus, D. (2012). Vivre/Leben/Vivere. Uno studio interdisciplinare sulle condizioni di vita degli ultrasessantacinquenni in Svizzera. *Dati. Statistiche e società*, XII(1), 114–117.
* Spahni, S., Morselli, D., Perrig-Chiello, P., & Bennett, K. M. (2015). Patterns of psychological adaptation to spousal bereavement in old age. *Gerontology*, 61(5), 456–468. [doi:10.1159/000371444](https://www.doi.org/10.1159/000371444)
* Tholomier, A. C. C. (2016). Fragilité et dépendance parmi les retraités suisses: effet du sexe, de l’âge et du statut social. In S. Pennec, C. Girard, & J.-P. Sanderson (Eds.) *Trajectoires et âges de la vie* (pp. 1–11). Paris: AIDELF. Retrieved from https://www.erudit.org/fr/livres/actes-des-colloques-de-lassociation-internationale-des-demographes-de-langue-francaise/trajectoires-ages-vie-selection-darticles-issus-travaux-presentes-au-xviiie--978-2-9521220-5-4/
* Widmer, É., & Girardin, M. (2016). Actively generating one’s family: How elders shape their family configurations. In E. Scabini & G. Rossi (Eds.), *L’allungamento della vita: Una risorsa per la famiglia, un’opportunità per la società* (pp. 85–104). Milano: Vita e Pensiero.
* Widmer, É., Girardin, M., & Ludwig, C. (2018). Conflict Structures in Family Networks of Older Adults and Their Relationship With Health-Related Quality of Life. *Journal of Family Issues*, 39(6), 1573–1597. [doi:10.1177/0192513X17714507](https://www.doi.org/10.1177/0192513X17714507)
* Widmer, É., & Spini, D. (2017). Misleading Norms and Vulnerability in the Life Course: Definition and Illustrations. *Research in Human Development*, 14(1), 52–67. [doi:10.1080/15427609.2016.1268894](https://www.doi.org/10.1080/15427609.2016.1268894)
