# Vivre - Leben - Vivere

«[Vivre - Leben - Vivere (VLV)](https://cigev.unige.ch/vlv/)» est une enquête transversale sur les personnes âgées dans les cantons de Bâle, Berne, Genève, Tessin et Valais, réalisée en 2011-2012 par le CIGEV (Centre interfacultaire de gérontologie et d'études des vulnérabilités).

Ce projet a été financé par le Fonds national suisse de la recherche scientifique ([FNS](http://www.snf.ch/)) à travers un subside [Sinergia](http://www.snf.ch/fr/encouragement/programmes/sinergia/), par le Pôle de recherche national *LIVES – Surmonter la vulnérabilité: perspective du parcours de vie* ([PRN LIVES](https://www.lives-nccr.ch/)), et par [Pro Senectute](https://www.prosenectute.ch/).

## Pour commencer

Nous proposons ici un jeu de données mis à jour et corrigé en 2019, ainsi qu'une documentation complète. La liste des fichiers disponibles est [détaillée ci-dessous](#contenu), et la documentation se trouve dans le fichier [trans_2011_doc.md](trans_2011_doc.md).

Les données de l'enquête ont été déposées auprès de [FORS](https://forscenter.ch/) en 2017. Le jeu de données présenté ici a fait l'objet de corrections entre 2017 et 2019.

En clonant ce dépôt, vous vous engagez à respecter la [licence d'utilisation](LICENSE) et à citer [les auteur·e·s](#auteures) et le titre de l'enquête ([voir la citation proposée](#citation)).

### Citation

(FR)
> Oris, M. (dir.). (2011). *Vivre - Leben - Vivere* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Oris, M. (Dir.). (2011). *Vivre - Leben - Vivere* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

### Prérequis

Vous pouvez télécharger les données fournies par le CIGEV et les utiliser librement, selon la [licence d'utilisation](LICENSE). Afin de faire profiter les autres utilisateurs/trices de vos corrections éventuelles, utilisez Git: voir le livre [Pro Git](https://git-scm.com/book/fr/v2) pour bien commencer.

Le [jeu de données](trans_2011_data.csv) est fourni au format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values), mais peut être importé dans [SPSS](https://fr.wikipedia.org/wiki/SPSS) grâce à la [syntaxe d'importation](trans_2011_data.sps) qui l'accompagne. La [documentation](trans_2011_doc.md) est fournie sous la forme d'un fichier [MD](https://fr.wikipedia.org/wiki/Markdown). Le [codebook](trans_2011_codebook.ods) a été exporté depuis SPSS et est fourni au format [OpenDocument](https://fr.wikipedia.org/wiki/OpenDocument). Les questionnaires sont au format [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format).

### Installation

Vous pouvez copier le jeu de données et la documentation en le téléchargeant depuis ce site (voir lien «Download» en haut à droite de la liste des fichiers) ou en clonant le dépôt:

```
git clone git@gitlab.unige.ch:cigev/trans_2011_vlv1.git
```

Pour importer les données dans SPSS, utilisez la syntaxe fournie (trans_2011_data.sps) et modifiez la ligne appelant le fichier CSV en indiquant le chemin complet du fichier.

## Contenu

* [LICENSE](LICENSE) – Licence d'utilisation des données
* [README.md](README.md) – Indications de base (ce fichier)
* [trans_2011_brochure_de.pdf](trans_2011_brochure_de.pdf) – Brochure, présentation texte (version allemande)
* [trans_2011_brochure_fr.pdf](trans_2011_brochure_fr.pdf) – Brochure, présentation texte (version française)
* [trans_2011_brochure_it.pdf](trans_2011_brochure_it.pdf) – Brochure (version italienne)
* [trans_2011_codebook.ods](trans_2011_codebook.ods) – Codebook QAA + QFF au format OpenDocument (feuille de calcul)
* [trans_2011_codebook_proxy.ods](trans_2011_codebook_proxy.ods) – Codebook QP au format OpenDocument (feuille de calcul)
* [trans_2011_data.csv](trans_2011_data.csv) – Jeu de données QAA + QFF au format CSV
* [trans_2011_data.sps](trans_2011_data.sps) – Syntaxe d'importation du jeu de données QAA + QFF au format SPSS
* [trans_2011_data_proxy.csv](trans_2011_data_proxy.csv) – Jeu de données QP au format CSV
* [trans_2011_data_proxy.sps](trans_2011_data_proxy.sps) – Syntaxe d'importation du jeu de données QP au format SPSS
* [trans_2011_doc.md](trans_2011_doc.md) – Documentation et rapport de traitement au format MD
* [trans_2011_doc_table1.md](trans_2011_doc_table1.md) – Tableau 1 (pondération: taille des échantillons et des populations) au format MD
* [trans_2011_doc_table2.md](trans_2011_doc_table2.md) – Tableau 2 (pondération: coefficients de pondération par classe d'âge et sexe) au format MD
* [trans_2011_fc_de.pdf](trans_2011_fc_de.pdf) – Fiche de contact (version allemande)
* [trans_2011_fc_fr.pdf](trans_2011_fc_fr.pdf) – Fiche de contact (version française)
* [trans_2011_fc_it.pdf](trans_2011_fc_it.pdf) – Fiche de contact (version italienne)
* [trans_2011_fe_de.pdf](trans_2011_fe_de.pdf) – Feuillet d'entretien (version allemande)
* [trans_2011_fe_fr.pdf](trans_2011_fe_fr.pdf) – Feuillet d'entretien (version française)
* [trans_2011_fe_it.pdf](trans_2011_fe_it.pdf) – Feuillet d'entretien (version italienne)
* [trans_2011_lc_it.pdf](trans_2011_lc_it.pdf) – Lettre de contact (version italienne)
* [trans_2011_planches_de.pdf](trans_2011_planches_de.pdf) – Planches (version allemande)
* [trans_2011_planches_fr.pdf](trans_2011_planches_fr.pdf) – Planches (version française)
* [trans_2011_planches_it.pdf](trans_2011_planches_it.pdf) – Planches (version italienne)
* [trans_2011_quest_cv_de.pdf](trans_2011_quest_cv_de.pdf) – Calendriers de vie (version allemande)
* [trans_2011_quest_cv_fr.pdf](trans_2011_quest_cv_fr.pdf) – Calendriers de vie (version française)
* [trans_2011_quest_cv_it.pdf](trans_2011_quest_cv_it.pdf) – Calendriers de vie (version italienne)
* [trans_2011_quest_qaa_de.pdf](trans_2011_quest_qaa_de.pdf) – Questionnaire, 1ère partie (auto-administré, version allemande)
* [trans_2011_quest_qaa_fr.pdf](trans_2011_quest_qaa_fr.pdf) – Questionnaire, 1ère partie (auto-administré, version française)
* [trans_2011_quest_qaa_it.pdf](trans_2011_quest_qaa_it.pdf) – Questionnaire, 1ère partie (auto-administré, version italienne)
* [trans_2011_quest_qff_de.pdf](trans_2011_quest_qff_de.pdf) – Questionnaire, 2e partie (face-à-face, version allemande)
* [trans_2011_quest_qff_fr.pdf](trans_2011_quest_qff_fr.pdf) – Questionnaire, 2e partie (face-à-face, version française)
* [trans_2011_quest_qff_it.pdf](trans_2011_quest_qff_it.pdf) – Questionnaire, 2e partie (face-à-face, version italienne)
* [trans_2011_quest_qp_de.pdf](trans_2011_quest_qp_fde.pdf) – Questionnaire proxy (version allemande)
* [trans_2011_quest_qp_fr.pdf](trans_2011_quest_qp_fr.pdf) – Questionnaire proxy (version française)
* [trans_2011_quest_qp_it.pdf](trans_2011_quest_qp_it.pdf) – Questionnaire proxy (version italienne)

## Contribuer

Si vous désirez contribuer à l'amélioration de ce jeu de données, de sa documentation, ou laisser des commentaires, vous pouvez contacter le CIGEV à l'adresse: [cigev-data@unige.ch](mailto:cigev-data@unige.ch). Le système Git vous permettra de proposer des modifications que nous pourrons valider, documenter et partager.

Pour toute utilisation des données (analyse, comparaison, enseignement, publication...), nous vous serions reconnaissants de bien vouloir nous informer: [cigev-data@unige.ch](mailto:cigev-data@unige.ch).

## Auteur·e·s

* **Michel Oris** – *Requérant principal*
* **Stefano Cavalli** – *Chef de projet*
* **Catherine Ludwig** – *Cheffe de projet*
* **Jean-François Bickel** – *Co-requérant*
* **Claudio Bolzman** – *Co-requérant*
* **Alessandra Canuto** – *Co-requérante*
* **Dominique Joye** – *Co-requérant*
* **Christophe Luthy** – *Co-requérant*
* **Pasqualina Perrig-Chiello** – *Co-requérante*
* **Dario Spini** – *Co-requérant*
* **Éric Widmer** – *Co-requérant*

### Collaboratrices et collaborateurs

* **Marie Baeriswyl**
* **Christine Cedraschi**
* **Étienne Christe**
* **Nora Dasoki**
* **Daniela Dus**
* **Aline Duvoisin**
* **Delphine Fagot**
* **Rainer Gabriel**
* **Myriam Girardin**
* **Julia Henke**
* **François Hermann**
* **Sara Hutchison**
* **Laure Kaeser**
* **Matthias Kliegel**
* **Katja Margelisch**
* **Barbara Masotti**
* **Christophe Monnot**
* **Marthe Nicolet**
* **Alessandra Rosciano**
* **Stefanie Spahni**
* **Aude Tholomier**
* **Angelica Torres Florez**
* **France Weaver**
* **Kerstin Weber**

### Administration et technique

* **Nathalie Blanc**
* **Claire Grela**
* **Grégoire Métral**

## Licence

Ce projet est distribué selon les termes de la licence de CreativeCommons CC-BY-SA. Voir le fichier [LICENSE](LICENSE) pour plus de détails.

## Remerciements

Merci à toutes les personnes (notamment les enquêtrices et enquêteurs) qui ont également collaboré à cette recherche.
